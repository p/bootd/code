/*
 * $Id$
 */

#include "cconf.h"

void            cconf_check_sun_rpc();
void            cconf_check_libnet(void);
void            cconf_check_libpcap(void);

void
cconf_user()
{
	/*
	 * we require __STDC__ because we need basic ANSI compatibility
	 * (prototypes at least)
	 */
#ifndef __STDC__
	log(LOG_ERROR, "Your systems does not seem to be even basically ANSI compliant.\n"
	    "please use an ANSI compliant C compiler or try gcc\n");
	/* notreached */
#endif

	/*
	 * make sure we have all the ansi stuff. Some systems don't have all
	 * of them, so it is not fatal to only have some of them.
	 */
	if (!cconf_check_headers_ansi()) {
		log_notfound(LOG_WARN, NOTFOUND_UNSPEC, "ANSI headers have not been found");
	}
	/*
	 * check for POSIX.1 headers many systems don't have all of them (no
	 * warning)
	 */
	cconf_check_headers_posix1();

	cconf_check_header("machine/param.h");

	cconf_check_header("sys/socket.h");
	cconf_check_header("sys/syslimits.h");
	cconf_check_header("linux/limits.h");

	cconf_check_header("netinet/in.h");
	cconf_check_header("netinet/if_ether.h");
	cconf_check_header("netinet/ether.h");

	cconf_check_header("arpa/inet.h");

	cconf_check_header("net/if_types.h");
	cconf_check_header("net/if_dl.h");
	cconf_check_header("net/if_var.h");

	cconf_check_header("ifaddrs.h");

	cconf_check_type_field("struct sockaddr_in", "sin_len");

	/* 'replace' functions */
	cconf_check_func("strncpy", NULL);
	cconf_check_func("strlcpy", NULL);
	cconf_check_func("strncat", NULL);
	cconf_check_func("strlcat", NULL);

	/* specific stuff */
	cconf_check_sun_rpc();
	cconf_check_libpcap();
	cconf_check_libnet();

	cconf_check_func("getifaddrs", NULL);
	cconf_write_config_h("../../include/config.h");
	cconf_write_makefile("../../Makefile.out", "../../Makefile.in");
}

void
cconf_user_init()
{
	cconf_table_string_put(CCONF_SET, "libnet-dir", "");
}

void
cconf_user_help()
{
	msg_console(" %-20s %s [%s]\n", "--libnet-dir=DIR", "directory containing libnet",
		    cconf_table_string_get(CCONF_SET, "libnet-dir"));
}

void
cconf_check_sun_rpc()
{
	/* check rpc stuff */
	if (!cconf_check_header("rpc/rpc.h")) {
		log_notfound(LOG_ERROR, NOTFOUND_HEADER, "rpc/rpc.h");
	}
	if (cconf_check_func("svc_dg_create", NULL)) {
		cconf_define_have("sun_rpc_new", 1);
		cconf_define_have("sun_rpc", 1);
	} else {
		cconf_define_have("sun_rpc_new", 0);
		if (cconf_check_func("getrpcport", NULL)) {
			cconf_define_have("sun_rpc", 1);
		} else {
			cconf_define_have("sun_rpc", 0);
		}
	}
	cconf_check_type_def("rpcprog_t", "unsigned long");
	cconf_check_type_def("rpcvers_t", "unsigned long");
}

void
cconf_check_libnet()
{
	char            libnet_config[2048];
	char            buf[2048];
	char            stdoutbuf[2048];
	int             i,l;
	struct stat	sb;
	char*		searchpaths[] = { "/usr/local", "/usr/pkg", "", NULL };

	/* check for libnet */
	msg_quest_checking_for("libnet-config");

	strcpy(libnet_config, cconf_table_string_get(CCONF_SET, "libnet-dir"));
	if (strcmp(libnet_config, "")) {
		sprintf(buf, "%s/bin/libnet-config", libnet_config);
		l = cconf_try_run(buf, NULL, 0, NULL, NULL, NULL, NULL);
	} else {
		/* try to search it ourselves */
		for (l=0,i=0;searchpaths[i];i++) {
			strcpy(libnet_config, searchpaths[i]);
			if (strcmp(libnet_config, "")) {
				sprintf(buf, "%s/bin/libnet-config", libnet_config);
			} else {
				strcpy(buf, "libnet-config");
			}
			if (cconf_try_run(buf, NULL, 0, NULL, NULL, NULL, NULL)) {
				l=1;
				if (strcmp(libnet_config, "")) {
					msg_console("(in %s) ", libnet_config);
				}
				break;
			}
		}
	}
	msg_ans_found(l);
	if (l=0) {
		log_notfound(LOG_ERROR, NOTFOUND_PROG, "libnet-config");
		/* notreached */
	}

	if (strcmp(libnet_config, "")) {
		sprintf(buf, "-I%s/include", libnet_config);
		cconf_setenv_app("CFLAGS", buf);
		sprintf(buf, "-L%s/lib", libnet_config);
		cconf_setenv_app("LDFLAGS", buf);
		strcat(libnet_config, "/bin/libnet-config");
	} else {
		strcpy(buf, "libnet-config");
	}

	sprintf(buf, "%s --defines", libnet_config);
	l = sizeof(stdoutbuf);
	cconf_try_run(buf, NULL, 0, stdoutbuf, &l, NULL, NULL);
	cconf_setenv_app("CFLAGS", stdoutbuf);

	sprintf(buf, "%s --cflags", libnet_config);
	l = sizeof(stdoutbuf);
	cconf_try_run(buf, NULL, 0, stdoutbuf, &l, NULL, NULL);
	cconf_setenv_app("CFLAGS", stdoutbuf);

	sprintf(buf, "%s --libs", libnet_config);
	l = sizeof(stdoutbuf);
	cconf_try_run(buf, NULL, 0, stdoutbuf, &l, NULL, NULL);
	cconf_setenv_app("LDADD", stdoutbuf);

	msg_quest_checking_for("libnet.h");
	log_level(LOG_WARN);
	if (cconf_check_header("libnet.h")) {
		log_level(LOG_INFO);
		msg_ans_yesno(1);
	} else {
		log_level(LOG_INFO);
		/*
		 * check whether we need LIBNET_GLIBC_HACK.
		 * struct ether_addr is defined in libnet-headers.h if __GLIBC__ is
		 * not defined (on most systems it is already defined in net/ethernet.h, leading to a conflict)
		 */

		if (
		    (!cconf_try_cc
		     ("#include \"confdefs.h\"\n#include <libnet.h>\nint main() { return 0; }\n", "cconf_check_header.c"))
		    &&
		    (cconf_try_cc
		     ("#define __GLIBC__ 1\n#include <libnet.h>\n#undef __GLIBC__\nint main() { return 0; }\n",
		      "cconf_check_header.c"))) {
			cconf_define_have("libnet.h",1);
			cconf_define("LIBNET_GLIBC_HACK", "1");
			msg_ans("yes (needs __GLIBC__ hack)");
		} else {
			log_notfound(LOG_ERROR, NOTFOUND_HEADER, "libnet.h");
			/* notreached */
		}
	}
	if (!cconf_check_func("libnet_open_link_interface", NULL)) {
		log_notfound(LOG_ERROR, NOTFOUND_LIBRARY, "libnet");
		/* notreached */
	}
}

void
cconf_check_libpcap()
{
	/* check for libpcap */
	if (!cconf_check_header("pcap.h")) {
		log_notfound(LOG_ERROR, NOTFOUND_HEADER, "pcap.h");
		/* notreached */
	}
	if (!cconf_check_func("pcap_open_live", "-lpcap", NULL)) {
		log_notfound(LOG_ERROR, NOTFOUND_LIBRARY, "libpcap");
		/* notreached */
	}
	/* not all versions of libpcap have this */
	cconf_check_func("pcap_freecode", NULL);
}
