/*
 * $Id$
 */

#include "bootd.h"

#include "bootparam_prot.h"

bp_whoami_res  *
bootparamproc_whoami_1(listener_t * l, struct sockaddr_in * from, struct sockaddr_in * to, bp_whoami_arg *
		       whoami);
bp_getfile_res *
bootparamproc_getfile_1(listener_t * l, struct sockaddr_in * from, struct sockaddr_in * to, bp_getfile_arg
			* getfile);

void
bootparam_service_init()
{
	service_t      *s;

	s = service_new();
	s->name = "bootparam";
	s->id = SERVICE_BOOTPARM;
	s->service = bootparamprog_1;
	s->d.rpc_udp.prognum = BOOTPARAMPROG;
	s->d.rpc_udp.versnum = BOOTPARAMVERS;
	service_add(s);
}

void
bootparamprog_1(struct svc_req * rqstp, SVCXPRT * transp)
{
	listener_t     *l;
	struct sockaddr_in sai_from, sai_to;
	struct sockaddr_in *ps_in;
	int             s_inl;
	union {
		bp_whoami_arg   bootparamproc_whoami_1_arg;
		bp_getfile_arg  bootparamproc_getfile_1_arg;
	}               argument;
	void           *result;
	bool_t(*xdr_argument) (), (*xdr_result) ();
	void           *(*local) ();
	int             fd;

	/* get the associated listener (ie identify callee) */
#ifdef HAVE_SUN_RPC_NEW
	fd = transp->xp_fd;
#else
	fd = transp->xp_sock;
#endif
	if ((l = listener_search_by_fd(fd)) == NULL) {
		log(LOG_WARN | LOG_DEV,
		    "bootparamprog: listener_search_by_fd() failed");
		svcerr_systemerr(transp);
	}
	s_inl = sizeof(struct sockaddr_in);
	if (getsockname(l->fd, (struct sockaddr *) & (sai_to), &s_inl) == -1) {
		log(LOG_WARN | LOG_DEV,
		    "bootparamprog: getsockname(): %s", strerror(errno));
		svcerr_systemerr(transp);
	}
	/* identify the caller */
	if ((ps_in = svc_getcaller(transp)) == NULL) {
		log(LOG_WARN | LOG_DEV,
		    "bootparamprog: svc_getrpccaller() failed");
		svcerr_systemerr(transp);
	}
	memcpy(&(sai_from), ps_in, sizeof(struct sockaddr_in));

	switch (rqstp->rq_proc) {
	case NULLPROC:
		log(LOG_INFO | LOG_DEV, "bootparamprog: NULLPROC from %s:%s",
		    inet_ntoa(ps_in->sin_addr), ntohs(ps_in->sin_port));
		svc_sendreply(transp, xdr_void, NULL);
		return;
	case BOOTPARAMPROC_WHOAMI:
		xdr_argument = xdr_bp_whoami_arg;
		xdr_result = xdr_bp_whoami_res;
		local = (void *(*) ())bootparamproc_whoami_1;
		break;
	case BOOTPARAMPROC_GETFILE:
		xdr_argument = xdr_bp_getfile_arg;
		xdr_result = xdr_bp_getfile_res;
		local = (void *(*) ())bootparamproc_getfile_1;
		break;
	default:
		log(LOG_WARN | LOG_DEV,
		    "bootparamprog: invalid proc %d from %s:%s",
		    rqstp->rq_proc, inet_ntoa(ps_in->sin_addr), ntohs(ps_in->sin_port));
		svcerr_noproc(transp);
		return;
	}

	/* extract the arguments */
	memset(&argument, 0, sizeof(argument));
	if (!svc_getargs(transp, xdr_argument, (char *)&argument)) {
		log(LOG_WARN | LOG_DEV,
		    "bootparamprog: error decoding packet from %s:%s",
		    inet_ntoa(ps_in->sin_addr), ntohs(ps_in->sin_port));
		svcerr_decode(transp);
		return;
	}
	/* call the procedure */
	if ((result = (*local) (l, &sai_from, &sai_to, &argument)) == NULL) {
		svcerr_auth(transp, AUTH_FAILED);
	} else if (!svc_sendreply(transp, xdr_result, result)) {
		log(LOG_WARN | LOG_DEV, "error sending bootparam reply");
		svcerr_systemerr(transp);
	}
	if (!svc_freeargs(transp, xdr_argument, (char *)&argument)) {
		log(LOG_WARN | LOG_DEV, "unable to free arguments");
	}
	return;
}

bp_whoami_res  *
bootparamproc_whoami_1(listener_t * l, struct sockaddr_in * from, struct sockaddr_in * to, bp_whoami_arg * whoami)
{
	char            s_from[32];
	char            s_to[32];
	char		s_ip[32];
	host_t         *h;
	static bp_whoami_res result;
	ipaddr_t*pip;

	pip = (ipaddr_t*) & (whoami->client_address.bp_address_u);

	/* log request */
	str_sai(from, sizeof(s_from), s_from);
	str_sai(to, sizeof(s_to), s_to);
	str_ip(pip, sizeof(s_ip), s_ip);
	log(LOG_INFO | LOG_BOOT, "bootparam whoami request from %s to %s\n"
	    "  ipaddress %s", s_from, s_to, s_ip);

	/* check config */
	if ((h = util_ip2host(pip)) == NULL) {
		log(LOG_INFO | LOG_BOOT,
		    "bootparam whoami request from host %s denied (client unknown)",
		    s_from);
		return NULL;
	}
	if ((h->service & SERVICE_BOOTPARM) == 0) {
		log(LOG_INFO | LOG_BOOT,
		    "bootparam whoami request from host %s denied (bootparam not allowed for this host)",
		    h->name);
		return NULL;
	}
	/* fill in result */
	result.client_name = h->name;
	result.domain_name = "";/* XXX fix this? */
	result.router_address.address_type = IP_ADDR_TYPE;
	memcpy(&(result.router_address.bp_address_u), &(l->ip), sizeof(struct in_addr));

	/* log response */
	str_ip(&l->ip, sizeof(s_ip), s_ip);
	log(LOG_INFO | LOG_BOOT, "bootparam whoami reply from %s to %s\n"
	    "  client name %s, domain name %s, router address %s",
	    s_to, s_from,
	    result.client_name, result.domain_name, s_ip);
	return &result;
}

bp_getfile_res *
bootparamproc_getfile_1(listener_t * l, struct sockaddr_in * from, struct sockaddr_in * to, bp_getfile_arg * getfile)
{
	char            s_from[32];
	char            s_to[32];
	char		s_ip[32];
	host_t         *h;
	file_t         *f;
	static bp_getfile_res result;

	/* log request */
	str_sai(from, sizeof(s_from), s_from);
	str_sai(to, sizeof(s_to), s_to);
	log(LOG_INFO | LOG_BOOT, "bootparam getfile request from %s to %s\n"
	    "  client_name %s, file_id %s",
	    s_from, s_to,
	    getfile->client_name, getfile->file_id);

	/* check config */
	if ((h = util_name2host(getfile->client_name)) == NULL) {
		log(LOG_INFO | LOG_BOOT,
		    "bootparam getfile request from host %s denied (client unknown)",
		    s_from);
		return NULL;
	}
	if ((h->service & SERVICE_BOOTPARM) == 0) {
		log(LOG_INFO | LOG_BOOT,
		    "bootparam getfile request from host %s denied (bootparam not allowed for this host)",
		    h->name);
		return NULL;
	}
	/* fill in result */
	if ((f = util_name2file(h, SERVICE_BOOTPARM, getfile->file_id, 0)) == NULL) {
		return NULL;
	}
	result.server_name = f->server;	/* XXX fix this: server must be more
					 * clearly defined in config */
	result.server_path = f->path;
	result.server_address.address_type = IP_ADDR_TYPE;
	memcpy(&(result.server_address.bp_address_u), &(l->ip), sizeof(struct in_addr));	/* XXX fix this */

	/* log response */
	str_ip(&l->ip, sizeof(s_ip), s_ip);
	log(LOG_INFO | LOG_BOOT, "bootparam getfile reply from %s to %s\n"
	    "  server name %s, server path %s, server address %s",
	    s_to, s_from,
	    result.server_name, result.server_path, s_ip);
	return &result;
}
