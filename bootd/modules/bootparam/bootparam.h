/*
 * $Id$
 */

#ifndef _BOOTPARAM_H
#define _BOOTPARAM_H

/*
 * SUN rpc.bootparam Protocol
 */

void            bootparamprog_1(struct svc_req * rqstp, SVCXPRT * transp);
void            bootparam_service_init();

#endif
