/*
 * $Id$
 */

#define CCONF_C
#include "cconf.h"

#ifdef __STDC__

static void     i_table_iterator_config_h(const char *key, const void *value, void *buf);
static void     i_table_iterator_env(const char *key, const void *value, void *buf);

#else				/* __STDC__ */

static void     i_table_iterator_config_h();
static void     i_table_iterator_env();

#endif				/* !__STDC__ */

#ifdef __STDC__
void
cconf_write_config_h(const char *filename)
#else
void
cconf_write_config_h(filename)
	char           *filename;
#endif
{
	int             fd;

	log_func_start("cconf_write_config_h", "%s", filename);
	if ((fd = open(filename, O_CREAT | O_TRUNC | O_WRONLY, 0777)) == -1) {
		cconf_log(LOG_ERROR | LOG_ERRNO, "open(%s, CREAT|TRUNC|WRONLY, 0777)\n", filename);
		/* notreached */
	}
	cconf_table_iterate(CCONF_DEF, i_table_iterator_config_h, (void *)&fd);
	close(fd);
	log_func_end("cconf_write_config_h", "");
}

#ifdef __STDC__
static void
i_table_iterator_config_h(const char *key, const void *value, void *buf)
#else
static void
i_table_iterator_config_h(key, value, buf)
	char           *key;
	void           *value;
	void           *buf;
#endif
{
	char            b[512];
	int             len;

	if (key == NULL) {
		return;
	}
	len = strlen(key);
	if (value == NULL) {
		if (len > (sizeof(b) - 11)) {
			cconf_log(LOG_ERROR, "expression too long, ignored: #undef %s\n", key);
			/* notreached */
		}
		len = sprintf(b, "#undef %s\n", key);
	} else {
		if ((len + strlen(value)) > (sizeof(b) - 13)) {
			cconf_log(LOG_ERROR, "expression too long, ignored: #define %s %s\n", key, value);
			/* notreached */
		}
		len = sprintf(b, "#define %s %s\n", key, (char *)value);
	}
	if (write(*(int *)buf, b, len) != len) {
		cconf_log(LOG_ERROR | LOG_ERRNO, "write(%d)\n", len);
		/* notreached */
	}
}

#ifdef __STDC__
void
cconf_write_makefile(const char *filename, const char *template)
#else
void
cconf_write_env(filename, template)
	char           *filename;
	char           *template;
#endif
{
	int             fdout;
	int             fdin;
	char *bufin, *c;
	int pos_p, pos_c, len;
	struct stat statb;

	log_func_start("cconf_write_env", "%s", filename);
	if ((fdout = open(filename, O_CREAT | O_TRUNC | O_WRONLY, 0777)) == -1) {
		cconf_log(LOG_ERROR | LOG_ERRNO, "open(%s, CREAT|TRUNC|WRONLY, 0777)\n", filename);
		/* notreached */
	}

	cconf_table_iterate(CCONF_ENV, i_table_iterator_env, (void *) &fdout);
	if ((fdin = open(template, O_RDONLY)) == -1) {
		cconf_log(LOG_ERROR | LOG_ERRNO, "open(%s, RDONLY)\n", filename);
		/* notreached */
	}
	/* ok, assume it is not *that* big */
	bufin=malloc(statb.st_size);
	if (read(fdin, bufin, statb.st_size)==-1) {
		cconf_log(LOG_ERROR | LOG_ERRNO, "read(%s, %d)\n", template, statb.st_size);
		/* notreached */
	}
	pos_p=0;
	pos_c=0;
	do {
		if (bufin[pos_c]=='@') {
			len = pos_c-pos_p;
			if (write(fdout, bufin+pos_p, len)!=len) {
				cconf_log(LOG_ERROR | LOG_ERRNO, "write(%s, %s)\n", filename, len);
				/* notreached */
			}
			pos_p=pos_c+1;
			while ((pos_c<statb.st_size)&&(bufin[pos_c]!='@')) {
				pos_c++;
			}
			if (bufin[pos_c]=='@') {
				bufin[pos_c]=0;
			} else {
				cconf_log(LOG_ERROR, "unexpected end of file in template %s\n", template);
				/* notreached */
			}
			c=(char*)cconf_table_string_get(CCONF_ENV, bufin+pos_p);
			len=strlen(c);
			if (write(fdout, c, len)!=len) {
				cconf_log(LOG_ERROR | LOG_ERRNO, "write(%s, %s)\n", filename, len);
				/* notreached */
			}
			pos_p=pos_c+1;
		}
		pos_c++;
	}
	while (pos_c<statb.st_size);
	if (pos_c==statb.st_size) {
		len = pos_c-pos_p;
		if (write(fdout, bufin+pos_p, len)!=len) {
			cconf_log(LOG_ERROR | LOG_ERRNO, "write(%s, %s)\n", filename, len);
			/* notreached */
		}
	}
	close(fdin);
	close(fdout);
	log_func_end("cconf_write_env", "");
}

#ifdef __STDC__
static void
i_table_iterator_env(const char *key, const void *value, void *buf)
#else
static void
i_table_iterator_env(key, value, buf)
	char           *key;
	void           *value;
	void           *buf;
#endif
{
	char            b[512];
	int             len;

	if (key == NULL) {
		return;
	}
	len = strlen(key);
	if (value == NULL) {
		if (len > (sizeof(b) - 11)) {
			cconf_log(LOG_ERROR, "expression too long, ignored: #%s=\n", key);
			/* notreached */
		}
		len = sprintf(b, "#%s=\n", key);
	} else {
		if ((len + strlen(value)) > (sizeof(b) - 13)) {
			cconf_log(LOG_ERROR, "expression too long, ignored: %s=%s\n", key, value);
			/* notreached */
		}
		len = sprintf(b, "%s=%s\n", key, (char *)value);
	}
	if (write(*(int *)buf, b, len) != len) {
		cconf_log(LOG_ERROR | LOG_ERRNO, "write(%d)\n", len);
		/* notreached */
	}
}

#ifdef __STDC__
int
cconf_try_run(const char *command, char *stdinbuf, int stdinlen, char *stdoutbuf, int *stdoutlen, char *stderrbuf, int *stderrlen)
#else
int
cconf_try_run(command, stdinbuf, stdinlen, stdoutbuf, stdoutlen, stderrbuf, stderrlen)
	char           *command;
	char           *stdinbuf;
	int             stdinlen;
	char           *stdoutbuf;
	int            *stdoutlen;
	char           *stderrbuf;
	int            *stderrlen;
#endif
{
	int             pid;
	int             result;
	int             stdoutfd, stderrfd;
	int             l_stdoutlen, l_stderrlen;
	char            buf[2048];

	log_func_start("cconf_try_run", "%s", command);
	stdoutfd = cconf_file_tmp_fd_write(NULL, 0);
	stderrfd = cconf_file_tmp_fd_write(NULL, 0);
	pid = fork();
	switch (pid) {
	case -1:
		cconf_log(LOG_ERROR | LOG_ERRNO, "fork()\n");
		/* notreached */
		break;
	case 0:
		/* child */
		close(STDIN_FILENO);
		cconf_file_tmp_fd_write(stdinbuf, stdinlen);	/* -> STDIN_FILENO */
		close(STDOUT_FILENO);
		dup(stdoutfd);	/* -> STDOUT_FILENO */
		close(stdoutfd);
		close(STDERR_FILENO);
		dup(stderrfd);	/* -> STDERR_FILENO */
		close(stderrfd);

		/* run the command */
		exit(system(command) ? 1 : 0);
		/* notreached */
	default:
		/* parent */

		/* wait for child and set result according to exit status */
		wait(&result);
		if (WIFEXITED(result)) {
			result = (WEXITSTATUS(result) == 0) ? 1 : 0;
		} else {
			result = 0;
		}

		/* log stdout even if no buffer has been specified */
		if ((stdoutbuf == NULL) || (stdoutlen == NULL)) {
			stdoutlen = &l_stdoutlen;
			l_stdoutlen = sizeof(buf);
			stdoutbuf = buf;
		}
		/* read in stdout, and log if there was any output */
		*stdoutlen = cconf_file_tmp_fd_read(stdoutfd, stdoutbuf, *stdoutlen);
		if ((*stdoutlen) > 0) {
			stdoutbuf[(*stdoutlen) - 1] = 0;
			log_func_start("stdout", "");
			cconf_log(LOG_DEBUG, "%s", stdoutbuf);
			log_func_end("stdout", "");
		}
		/* log stderr even if no buffer has been specified */
		if ((stderrbuf == NULL) || (stderrlen == NULL)) {
			stderrlen = &l_stderrlen;
			l_stderrlen = sizeof(buf);
			stderrbuf = buf;
		}
		/* read in stderr, and log if there was any output */
		*stderrlen = cconf_file_tmp_fd_read(stderrfd, stderrbuf, *stderrlen);
		if ((*stderrlen) > 0) {
			stderrbuf[(*stderrlen) - 1] = 0;
			log_func_start("stderr", "");
			cconf_log(LOG_DEBUG, "%s", stderrbuf);
			log_func_end("stderr", "");
		}
		break;
	}
	log_func_end("cconf_try_run", "result=%d", result);
	return result;
}

#ifdef __STDC__
int
cconf_try_cc(const char *srctext, const char *name)
#else
int
cconf_try_cc(srctext, name)
	char           *srctext;
	char           *name;
#endif
{
	int             result;
	char            command[2048];

	log_func_start("cconf_try_cc", "%s", name);

	if (!cconf_file_write(name, srctext, strlen(srctext))) {
		cconf_log(LOG_ERROR, "cconf_try_cc: unable to write file\n");
		/* notreached */
	}
	sprintf(command, "%s %s -c %s", CC, CFLAGS, name);
	result = cconf_try_run(command, NULL, 0, NULL, NULL, NULL, NULL);

	log_func_end("cconf_try_cc", "result=%d", result);
	return result;
}

#ifdef __STDC__
int
cconf_try_link(const char *name,...)
#else
int
cconf_try_link(name, va_alist)
	char           *name;
va_dcl
#endif
{
	int             result;
	char            command[2048];
	va_list         ap;
	char           *arg;

	log_func_start("cconf_try_link", "%s", name);

	sprintf(command, "%s -o %s %s ", CC, name, LDFLAGS);
	VA_START(ap, name);
	while ((arg = VA_ARG(ap, char *))) {
		strcat(command, arg);
		strcat(command, " ");
	}
	VA_END(ap);
	strcat(command, LDADD);

	result = cconf_try_run(command, NULL, 0, NULL, NULL, NULL, NULL);

	log_func_end("cconf_try_link", "result=%d", result);
	return result;
}

#ifdef __STDC__
int
cconf_try_cpp(const char *srctext, char *stdoutbuf, int *stdoutlen)
#else
int
cconf_try_cpp(srctext, stdoutbuf, stdoutlen)
	char           *srctext;
	char           *stdoutbuf;
	int            *stdoutlen;
#endif
{
	int             result;
	char            command[2048];

	log_func_start("cconf_try_cpp", "");

	if (!cconf_file_write("cconf_try_cpp.c", srctext, strlen(srctext))) {
		cconf_log(LOG_ERROR, "cconf_try_cpp: unable to write file\n");
		/* notreached */
	}
	sprintf(command, "%s %s %s", CPP, CFLAGS, "cconf_try_cpp.c");
	result = cconf_try_run(command, NULL, 0, stdoutbuf, stdoutlen, NULL, 0);

	log_func_end("cconf_try_cpp", "result=%d", result);
	return result;
}

#ifdef __STDC__
int
cconf_file_word(const char *file, const char *word)
#else
int
cconf_file_word(file, word)
	char           *file;
	char           *word;
#endif
{
	int             fd;
	int             result;
	char            buf[4096];
	int             jt[256];
	int             i, blen, wlen, pos;

	log_func_start("cconf_file_word", "file=%s, word=%s", file, word);
	result = 0;

	/* huge improvement on long words or large files */
	wlen = strlen(word);
	for (i = 0; i < sizeof(jt); i++) {
		jt[i] = wlen;
	}
	for (i = 0; i < wlen; i++) {
		jt[(int)word[i]] = i;
	}

	if ((fd = open(file, O_RDONLY)) != -1) {
		pos = 0;
		i = 0;
		do {
			if ((blen = read(fd, buf, sizeof(buf))) < 0) {
				cconf_log(LOG_DEBUG | LOG_ERRNO, "read(%s)", file);
			}
			if (blen <= 0) {
				break;
			}
			i %= blen;
			while (i < blen) {
				if (word[pos] == buf[i]) {
					pos++;
					i++;
					if (pos >= wlen) {
						result = 1;
						blen = 0;
						break;
					}
				} else {
					pos = 0;
					i += jt[(int)word[pos]];
				}
			}
		} while (blen > 0);
		close(fd);
	} else {
		cconf_log(LOG_DEBUG | LOG_ERRNO, "open(%s, RDONLY)", file);
	}
	log_func_end("cconf_file_word", "result=%d", result);
	return result;
}

int
cconf_check_cc()
{
	int             result;
	char            buf[2048];
	int             buf_len;
	char            stdoutbuf[2048];
	int             stdout_len;
	char            stderrbuf[2048];
	int             stderr_len;
	char           *c;

	log_func_start("cconf_check_cc", "");

	msg_quest_checking_if("c compiler (%s %s) works", CC, CFLAGS);

	cconf_file_append_str(CCONF_CONFDEFS_H, "\n");
	result = cconf_try_cc("int main() { return 0; }\n", "cconf_check_cc.c");
	msg_ans_yesno(result);

	if (result) {
		msg_quest_checking_if("linker (%s %s %s) works", LD, LDFLAGS, LDADD);
		result = cconf_try_link("cconf_check_cc", "cconf_check_cc.o", NULL);
		msg_ans_yesno(result);
	}
	if (result) {
		msg_quest_checking_if("preprocessor (%s) works", CPP);
		buf_len = sizeof(buf);
		result = cconf_try_cpp("#define CCONF_CPP_TEST cconf_cpp_test\nCCONF;\nCCONF_CPP_TEST;\n", buf, &buf_len);
		if ((result == 0) || (strstr(buf, "CCONF") == NULL) || (strstr(buf, "CCONF_CPP_TEST") != NULL) || (strstr(buf, "cconf_cpp_test") == NULL)) {
			result = 0;
		}
		msg_ans_yesno(result);
	}
	if (result == 0) {
		cconf_log(LOG_ERROR, "c compiler/linker/preprocessor does not work!!!\n"
			  "please set CC, CFLAGS, CPP, LD, LDFLAGS and LDADD correctly\n"
		  "CC=%s, CFLAGS=%s\nCPP=%s\nLD=%s, LDFLAGS=%s, LDADD=%s\n",
			  CC, CFLAGS, CPP, LD, LDFLAGS, LDADD);
		/* notreached */
	}
	msg_quest_checking_if("cross compiling");
	result = !cconf_try_run("./cconf_check_cc", NULL, 0, NULL, NULL, NULL, NULL);
	msg_ans_yesno(result);
	cconf_define("cross compiling", result ? "1" : NULL);

	cconf_log(LOG_INFO, "sanity checking compiler");

	/* make sure it complains about missing includes */
	if (cconf_try_cc("#include \"nonexistant/this_header_file_does_not_exist.h\"\nint main() { return 0; }\n", "cconf_check_cc.c")) {
		cconf_log(LOG_ERROR, "\nc compiler does not complain about non-existing includes!!!\n"
			  "cconf will not work correctly\n");
		/* notreached */
	}
	msg_console(".");

	/* make sure it complains about undefined symbols */
	if (!cconf_try_cc("int cconf_check_cc_test();\nint main() { return cconf_check_cc_test(); }\n", "cconf_check_cc.c")) {
		cconf_log(LOG_ERROR, "\nunexpected c compiler error\n");
		/* notreached */
	}
	msg_console(".");

	if (cconf_try_link("cconf_check_cc", "cconf_check_cc.o", NULL)) {
		cconf_log(LOG_ERROR, "\nc compiler does not complain about undefined symbols!!!\n"
			  "cconf will not work correctly\n");
		/* notreached */
	}
	msg_console(".");

	/* make sure it complains about undefined types */
	if (cconf_try_cc("int main() { return sizeof(cconf_try_cc_nonexistanttype); }\n", "cconf_check_cc.c")) {
		cconf_log(LOG_ERROR, "\nc compiler does not complain about undefined types!!!\n"
			  "cconf will not work correctly\n");
		/* notreached */
	}
	msg_console(" ");
	msg_ans("ok");

	msg_quest_checking_for("compiler version");

	sprintf(buf, "%s -v", CC);
	stdout_len = sizeof(stdoutbuf);
	stderr_len = sizeof(stderrbuf);
	if (cconf_try_run(buf, NULL, 0, stdoutbuf, &stdout_len, stderrbuf, &stderr_len) && ((c = strstr(stderrbuf, "gcc")) != NULL)) {
		/* gcc */
		msg_ans(c);
		cconf_table_string_put_def(CCONF_ENV, "OPT", getenv("DEBUG"), "-O2 -fomit-frame-pointer -finline-functions -ffast-math");
		cconf_table_string_put_def(CCONF_ENV, "DEBUG", getenv("DEBUG"), "-g");
		cconf_table_string_put_def(CCONF_ENV, "WARN", getenv("WARN"), "-Wall -Werror");
	} else {
		sprintf(buf, "%s -version", CC);
		stdout_len = sizeof(stdoutbuf);
		stderr_len = sizeof(stderrbuf);
		if (cconf_try_run(buf, NULL, 0, stdoutbuf, &stdout_len, stderrbuf, &stderr_len) && (strstr(stdoutbuf, "MIPSpro") != NULL)) {
			/* SGI MIPSpro */
			msg_ans(stdoutbuf);
		} else {
			msg_ans("unknown");
		}
	}

	log_func_end("cconf_check_cc", "result=%d", result);
	return result;
}

//void
// i_check_cc_gcc()
// {
//int           result;
//if (cconf_try_cc("int main() {\n#ifdef __GNUC__\nreturn 0;\n#else\nreturn 1;\n#endif }\n", "cconf_check_cc.c") &&
//cconf_try_link("cconf_check_cc", "cconf_check_cc.o", NULL) &&
//cconf_try_run("./cconf_check_cc")) { } -D__GNUC__ = 2 - D__GNUC_MINOR__ = 95
// }
// }

#ifdef __STDC__
int
cconf_check_header(const char *header)
#else
int
cconf_check_header(header)
	char           *header;
#endif
{
	char            buf[512];
	int             result;

	log_func_start("cconf_check_header", "%s", header);
	msg_quest_checking_for("%s", header);

	if (cconf_defined_have(header, &result)) {
		msg_cached();
	} else {
		sprintf(buf, "#include \"%s\"\n#include <%s>\nint main() { return 0; }\n", CCONF_CONFDEFS_H, header);
		result = cconf_try_cc(buf, "cconf_check_header.c");
		if (result) {
			sprintf(buf, "#include <%s>\n", header);
			cconf_file_append_str(CCONF_CONFDEFS_H, buf);
		}
	}
	msg_ans_yesno(result);
	cconf_define_have(header, result);
	log_func_end("cconf_check_header", "result=%d", result);
	return result;
}

#ifdef __STDC__
int
cconf_check_headers(const char *desc,...)
#else
int
cconf_check_headers(desc, va_alist)
	char           *desc;
va_dcl
#endif
{
	int             result;
	char            source[4096];
	int             i, j;
	va_list         ap;
	char           *arg;
	log_func_start("cconf_check_headers", "%s", desc);
	msg_quest_checking_for("%s", desc);

	if (cconf_defined_have(desc, &result)) {
		msg_cached();
	} else {

		/* create source listing */
		i = sprintf(source, "#include \"%s\"\n", CCONF_CONFDEFS_H);

		VA_START(ap, desc);
		while ((arg = VA_ARG(ap, char *))) {
			if ((i + strlen(arg) + 30) > sizeof(source)) {
				cconf_log(LOG_ERROR, "cconf_check_headers: too many arguments\n");
				/* notreached */
			}
			i += sprintf(source + i, "#include <%s>\n", arg);
		}
		VA_END(ap);
		strcat(source + i, "int main() { return 0; }\n");

		/* try to compile */
		if ((result = cconf_try_cc(source, "cconf_check_headers.c")) != 0) {
			VA_START(ap, desc);
			while ((arg = VA_ARG(ap, char *))) {
				sprintf(source, "#include <%s>\n", arg);
				cconf_file_append_str(CCONF_CONFDEFS_H, source);
				cconf_define_have(arg, 1);
			}
			VA_END(ap);
		} else {
			/* ok, that failed, try them one by one */
			VA_START(ap, desc);
			i = 0;
			j = 0;
			while ((arg = VA_ARG(ap, char *))) {
				j++;
				if (!cconf_defined_have(arg, &result)) {
					msg_console(".");
					sprintf(source, "#include \"%s\"\n#include <%s>\nint main() { return 0; }\n", CCONF_CONFDEFS_H, arg);
					if (cconf_try_cc(source, "cconf_check_headers.c") != 0) {
						sprintf(source, "#include <%s>\n", arg);
						cconf_file_append_str(CCONF_CONFDEFS_H, source);
						cconf_define_have(arg, 1);
						i++;
					} else {
						cconf_define_have(arg, 0);
					}
				} else {
					i += result;
				}
			}
			msg_console("(%d of %d) ", i, j);
			result = 0;
		}
		cconf_define_have(desc, result);
	}
	msg_ans_yesno(result);
	log_func_end("cconf_check_headers", "result=%d", result);
	return result;
}

int
cconf_check_headers_ansi()
{
	return cconf_check_headers("ANSI headers", "assert.h", "ctype.h",
		     "errno.h", "float.h", "limits.h", "locale.h", "math.h",
		  "setjmp.h", "signal.h", "stdarg.h", "stddef.h", "stdio.h",
				   "stdlib.h", "string.h", "time.h", NULL);
}

int
cconf_check_headers_posix1()
{

	return cconf_check_headers("POSIX.1 headers", "cpio.h", "dirent.h",
	      "fcntl.h", "grp.h", "pwd.h", "tar.h", "termios.h", "unistd.h",
				   "utime.h", "sys/stat.h", "sys/times.h", "sys/types.h", "sys/utsname.h", "sys/wait.h", NULL);
}

int
cconf_check_headers_xpg3()
{
	return cconf_check_headers("XPG3 headers", "ftw.h", "langinfo.h", "nl_types.h", "regex.h", "search.h",
		   "ulimit.h", "sys/ipc.h", "sys/msg.h", "sys/sem.h", NULL);
}

#ifdef __STDC__
int
cconf_check_func(const char *func,...)
#else
int
cconf_check_func(func, va_alist)
	char           *func;
va_dcl
#endif
{
	char            buf[512];
	int             result;
	va_list         ap;
	char           *arg;
	CONST char     *ldadd;
	CONST char     *sldadd;

	log_func_start("cconf_check_func", "%s", func);
	msg_quest_checking_for("%s()", func);

	if (cconf_defined_have(func, &result)) {
		msg_cached();
	} else {
		sprintf(buf, "#undef %s\nchar %s();\nint main() { %s(); return 0; }\n", func, func, func);

		result = cconf_try_cc(buf, "cconf_check_func.c");
		if (result) {
			result = cconf_try_link("cconf_check_func", "cconf_check_func.o", NULL);
			if (!result) {
				ldadd = cconf_table_get(CCONF_ENV, "LDADD");
				sldadd = cconf_table_string_get(CCONF_ENV, "LDADD");

				VA_START(ap, func);
				while ((arg = VA_ARG(ap, char *))) {
					sprintf(buf, "%s %s", sldadd, arg);
					cconf_table_put(CCONF_ENV, "LDADD", buf);
					result = cconf_try_link("cconf_check_func", "cconf_check_func.o", NULL);
					if (result) {
						/* we found it, it's ok */
						break;
					}
				}
				VA_END(ap);

				cconf_table_put(CCONF_ENV, "LDADD", ldadd);

				if (result) {
					msg_console("(in %s) ", arg);
					cconf_table_string_put(CCONF_ENV, "LDADD", buf);
				}
			}
		}
		cconf_define_have(func, result);
	}
	msg_ans_yesno(result);
	log_func_end("cconf_check_func", "result=%d", result);
	return result;
}

#ifdef __STDC__
int
cconf_check_type(const char *type)
#else
int
cconf_check_type(type)
	char           *type;
#endif
{
	char            source[2048];
	int             result;

	log_func_start("cconf_check_type", "%s", type);
	msg_quest_checking_for("%s", type);

	if (cconf_defined_have(type, &result)) {
		msg_cached();
	} else {
		sprintf(source, "#include \"%s\"\nint main() { return sizeof(%s); }\n", CCONF_CONFDEFS_H, type);

		/* try to compile */
		result = cconf_try_cc(source, "cconf_check_type.c");
		cconf_define_have(type, result);
	}
	msg_ans_yesno(result);
	log_func_end("cconf_check_type", "result=%d", result);
	return result;
}

#ifdef __STDC__
void
cconf_check_type_def(const char *type, const char *def)
#else
void
cconf_check_type_def(type, def)
	char           *type;
	char           *def;
#endif
{
	char            source[2048];
	int             result;

	log_func_start("cconf_check_type_def", "%s", type);
	msg_quest_checking_for("%s", type);

	if (cconf_defined_have(type, &result)) {
		msg_cached();
		msg_ans_yesno(result);
	} else {
		sprintf(source, "#include \"%s\"\nint main() { return sizeof(%s); }\n", CCONF_CONFDEFS_H, type);

		/* try to compile */
		result = cconf_try_cc(source, "cconf_check_type.c");
		if (result) {
			msg_ans_yesno(result);
		} else {
			cconf_table_string_put(CCONF_DEF, type, def);
			msg_ans("yes (%s)", def);
		}
		cconf_define_have(type, result);
	}
	log_func_end("cconf_check_type_def", "result=%d", result);
}

void
cconf_check_std_types()
{
	/*
	 * check for standard types. the defaults are more or less
	 * reasonable, but we should (currently we don't) complain loud,
	 * because these are really, really system dependent, and there is no
	 * way for us to know
	 */

	cconf_check_type_def("u_int8_t", "unsigned char");
	cconf_check_type_def("int8_t", "signed char");
	cconf_check_type_def("u_int16_t", "unsigned short");
	cconf_check_type_def("int16_t", "signed short");
	cconf_check_type_def("u_int32_t", "unsigned int");
	cconf_check_type_def("int32_t", "signed int");
	cconf_check_type("u_int64_t");
	cconf_check_type("int64_t");

	cconf_check_type_def("caddr_t", "char *");
	cconf_check_type_def("daddr_t", "int");
	cconf_check_type_def("dev_t", "unsigned int");
	cconf_check_type_def("fixpt_t", "unsigned int");
	cconf_check_type_def("gid_t", "unsigned int");
	cconf_check_type_def("ino_t", "unsigned int");
	cconf_check_type_def("key_t", "long");
	cconf_check_type_def("mode_t", "unsigned short");
	cconf_check_type_def("nlink_t", "unsigned short");
	cconf_check_type_def("off_t", "long");
	cconf_check_type_def("pid_t", "int");
	cconf_check_type_def("rlim_t", "long");
	cconf_check_type_def("segsz_t", "long");
	cconf_check_type_def("swblk_t", "int");
	cconf_check_type_def("uid_t", "unsigned int");
	cconf_check_type_def("clock_t", "unsigned long");
	cconf_check_type_def("clockid_t", "int");
	cconf_check_type_def("ptrdiff_t", "int");
	cconf_check_type_def("rune_t", "int");
	cconf_check_type_def("size_t", "unsigned int");
	cconf_check_type_def("ssize_t", "int");
	cconf_check_type_def("time_t", "long");
	cconf_check_type_def("timer_t", "int");
	cconf_check_type_def("wchar_t", "int");
	cconf_check_type_def("va_list", "char *");
	cconf_check_type_def("socklen_t", "int");
}

#ifdef __STDC__
int
cconf_check_type_field(const char *type, const char *field)
#else
int
cconf_check_type_field(type, field)
	char           *type;
	char           *field;
#endif
{
	char            source[2048];
	char            define[128];
	int             result;

	log_func_start("cconf_check_type_field", "%s", type);
	msg_quest_checking_for("%s in %s", field, type);

	sprintf(define, "%s_%s", type, field);
	if (cconf_defined_have(define, &result)) {
		msg_cached();
	} else {
		sprintf(source, "#include \"%s\"\nint main() { %s a; char *b; b=(char*)&(a.%s); return 0; }\n", CCONF_CONFDEFS_H, type, field);

		/* try to compile */
		result = cconf_try_cc(source, "cconf_check_type_field.c");
		cconf_define_have(define, result);
	}
	msg_ans_yesno(result);
	log_func_end("cconf_check_type_field", "result=%d", result);
	return result;
}

#ifdef __STDC__
int
cconf_file_read(const char *name, void *buf, int buflen)
#else
int
cconf_file_read(name, buf, buflen)
	char           *name;
	void           *buf;
	int             buflen;
#endif
{
	int             fd;
	int             result;

	if ((name == NULL) || (buf == NULL) || (buflen <= 0)) {
		return 0;
	}
	if ((fd = open(name, O_RDONLY)) == -1) {
		cconf_log(LOG_DEBUG | LOG_ERRNO, "open(%s, RDONLY)\n", name);
		close(fd);
		return 0;
	}
	if ((result = read(fd, buf, buflen)) == -1) {
		cconf_log(LOG_DEBUG | LOG_ERRNO, "read(%s, %d)\n", name, buflen);
		close(fd);
		return 0;
	}
	close(fd);
	return result;
}

#ifdef __STDC__
int
cconf_file_write(const char *name, const void *buf, int buflen)
#else
int
cconf_file_write(name, buf, buflen)
	char           *name;
	void           *buf;
	int             buflen;
#endif
{
	int             fd;

	if (name == NULL) {
		return 0;
	}
	if ((fd = open(name, O_CREAT | O_TRUNC | O_WRONLY, 0777)) == -1) {
		cconf_log(LOG_DEBUG | LOG_ERRNO, "open(%s, CREAT|TRUNC|WRONLY, 0777)\n", name);
		return 0;
	}
	if ((buf == NULL) || (buflen <= 0)) {
		/* nothing to write */
		return 1;
	}
	if (write(fd, buf, buflen) != buflen) {
		cconf_log(LOG_DEBUG | LOG_ERRNO, "write(%s, %d)\n", name, buflen);
		close(fd);
		return 0;
	}
	close(fd);
	return 1;
}

#ifdef __STDC__
int
cconf_file_append(const char *name, const void *buf, int buflen)
#else
int
cconf_file_append(name, buf, buflen)
	char           *name;
	void           *buf;
	int             buflen;
#endif
{
	int             fd;

	if (name == NULL) {
		return 0;
	}
	if ((fd = open(name, O_CREAT | O_APPEND | O_WRONLY, 0777)) == -1) {
		cconf_log(LOG_DEBUG | LOG_ERRNO, "open(%s, CREAT|APPEND|WRONLY, 0777)\n", name);
		return 0;
	}
	if ((buf == NULL) || (buflen <= 0)) {
		/* nothing to write */
		return 1;
	}
	if (write(fd, buf, buflen) != buflen) {
		cconf_log(LOG_DEBUG | LOG_ERRNO, "write(%s, %d)\n", name, buflen);
		close(fd);
		return 0;
	}
	close(fd);
	return 1;
}

#ifdef __STDC__
int
cconf_file_append_str(const char *name, const char *str)
#else
int
cconf_file_append_str(name, str)
	char           *name;
	char           *str;
#endif
{
	return cconf_file_append(name, str, strlen(str));
}

#ifdef __STDC__
int
cconf_file_tmp_fd_read(int fd, void *buf, int buflen)
#else
int
cconf_file_tmp_fd_read(fd, buf, buflen)
	int             fd;
	void           *buf;
	int             buflen;
#endif
{
	int             result;

	if ((buf == NULL) || (buflen <= 0)) {
		return 0;
	}
	lseek(fd, 0, SEEK_SET);
	if ((result = read(fd, buf, buflen)) == -1) {
		cconf_log(LOG_DEBUG | LOG_ERRNO, "read(%d)\n", buflen);
		close(fd);
		return 0;
	}
	close(fd);
	return result;
}

#ifdef __STDC__
int
cconf_file_tmp_fd_write(const void *buf, int buflen)
#else
int
cconf_file_tmp_fd_write(buf, buflen)
	void           *buf;
	int             buflen;
#endif
{
	/*
	 * errors are fatal. if we can't even write a temp file, there's no
	 * sense continuing
	 */
	int             fd;
	int             i;
	char            filename[512];

	for (i = 0; i < 65536; i++) {
		sprintf(filename, "cconf_file_tmp_fd%04x", i);
		if ((fd = open(filename, O_RDWR | O_CREAT | O_EXCL, 0777)) == -1) {
			if (errno != EEXIST) {
				cconf_log(LOG_ERROR | LOG_ERRNO, "open(%s, RDWR | CREAT | EXCL)\n", filename);
				/* notreached */
			}
		} else {
			unlink(filename);
			if ((buf == NULL) || (buflen <= 0)) {
				return fd;
			} else {
				if (write(fd, buf, buflen) != buflen) {
					cconf_log(LOG_ERROR | LOG_ERRNO, "write(%d)\n", buflen);
					/* notreached */
				}
				lseek(fd, 0, SEEK_SET);
				return fd;
			}
		}
	}
	cconf_log(LOG_ERROR, "cconf_file_tmp_fd: unable to create file\n");
	/* notreached */
	return -1;
}
