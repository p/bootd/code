/*
 * $Id$
 */

#ifndef _CONFIG_H
#define _CONFIG_H

void            usage();
void            conf_args(int argc, char *argv[]);
void            conf_file(char *filename);
#endif
