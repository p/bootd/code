/*
 * $Id$
 */

#ifndef _BOOTD_H
#define _BOOTD_H

#include "config.h"

/* ANSI headers */
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_STDARG_H
#include <stdarg.h>
#endif
#ifdef HAVE_TIME_H
#include <time.h>
#endif

/* POSIX.1 headers */
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#include <sys/time.h>

#include <sys/socket.h>
#include <sys/sysctl.h>
#include <netinet/in.h>
#ifdef HAVE_NETINET_IF_ETHER_H
#include <netinet/if_ether.h>
#endif
#ifdef HAVE_NETINET_ETHER_H
#include <netinet/ether.h>
#endif
#include <net/if.h>
#ifdef HAVE_NET_IF_TYPES_H
#include <net/if_types.h>
#endif
#ifdef HAVE_NET_IF_DL_H
#include <net/if_dl.h>
#endif
#ifdef HAVE_NET_IF_VAR_H
#include <net/if_var.h>
#endif
#include <net/route.h>
#ifdef HAVE_IFADDRS_H
#include <ifaddrs.h>
#endif
#include <pcap.h>

#ifdef LIBNET_GLIBC_HACK
#define __GLIBC__ 1
#include <libnet.h>
#undef __GLIBC__
#else
#include <libnet.h>
#endif

#include <rpc/rpc.h>

#include "genutil.h"
#include "ngenutil.h"
#include "listener.h"
#include "device.h"
#include "service.h"
#include "host.h"
#include "util.h"
#include "conf.h"
#include "global.h"

#include "rarp.h"
#include "rmp.h"
#include "tftp.h"
#include "bootparam.h"
#include "dhcp.h"

#define PREFIX "/server/netboot/src/bootd"
#define CONF "/etc/bootd.conf"
#endif
