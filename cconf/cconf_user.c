/*
 * $Id$
 */

#include "cconf.h"

void
cconf_user()
{
/*
	cconf_check_header("foo.h");
	cconf_check_header("foo.h");
	cconf_check_header("paths.h");
	cconf_check_headers_ansi();
	cconf_check_headers_ansi();
	cconf_check_headers_posix1();
	cconf_check_headers_posix1();
	cconf_check_headers_xpg3();
	cconf_check_headers_xpg3();

	cconf_check_func("strerror", NULL);
	cconf_check_func("strerror", NULL);

	cconf_check_type("struct doesnotexist");
	cconf_check_type("struct doesnotexist");
	cconf_check_type("struct tm");
	cconf_check_type("struct tm");

	cconf_check_std_types();
*/

/*
	log_notfound(LOG_DEBUG, NOTFOUND_HEADER, "blabla.h");
	log_notfound(LOG_INFO, NOTFOUND_FUNCTION, "blabla()");
	log_notfound(LOG_WARN, NOTFOUND_LIBRARY, "libblabla");
	log_notfound(LOG_ERROR, NOTFOUND_TYPE, "struct blabla");
*/

	cconf_check_func("pcap_open_live", "-lpcap", NULL);
	cconf_check_func("pcap_open_offline", NULL);
	cconf_check_func("pcap_freecode", NULL);

	cconf_write_config_h("config.h");
	cconf_write_env("Makefile.inc");
}

void
cconf_user_init()
{
	cconf_table_string_put(CCONF_SET, "libnet-dir", "");
}

void
cconf_user_help()
{
	msg_console(" %-20s %s [%s]\n", "--libnet-dir=DIR", "directory containing libnet", cconf_table_string_get(CCONF_SET, "libnet-dir"));
}
