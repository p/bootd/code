/*
 * $Id$
 */

#ifndef CCONF_I_H
#define CCONF_I_H

#ifdef __STDC__			/* ANSI C */

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define VA_START(ap, last) va_start(ap, last)
#define VA_ARG(ap, type) va_arg(ap, type)
#define VA_END(ap) va_end(ap)

#define CONST const

#else				/* __STDC__ */

#include <varargs.h>
#define VA_START(ap, fixed) va_start(ap)
#define VA_ARG(ap, type) va_arg(ap, type)
#define VA_END(ap) va_end(ap)

/* with a K&R compiler, you probably don't have prototypes for these */
extern char    *malloc();
extern void     free();

extern char    *strerror();
extern int      errno;

extern char    *getenv();
extern int      sprintf();

#ifndef NULL
#define NULL (0)

#define CONST
#endif
#ifndef STDIN_FILENO
#define STDIN_FILENO 0
#endif
#ifndef STDOUT_FILENO
#define STDOUT_FILENO 1
#endif
#ifndef STDERR_FILENO
#define STDERR_FILENO 2
#endif

/* XXX */
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#endif				/* !__STDC__ */


/*
 * POSIX.1 for now, just assume we have these...
 */
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>

#if ((('1'-'0')!=1)||(('9'-'0')!=9)||(('Z'-'A')!=('z'-'a')))
please fix ctype macros
#endif

#if ((' ' & 0x0ff) == 0x020)
#define ISLOWER(c) ('a' <= (c) && (c) <= 'z')
#define ISUPPER(c) ('A' <= (c) && (c) <= 'Z')
#define TOUPPER(c) (ISLOWER(c) ? 'A' + ((c) - 'a') : (c))
#else
#define ISLOWER(c) (('a' <= (c) && (c) <= 'i') \
	|| ('j' <= (c) && (c) <='r') \
	|| ('s' <= (c) && (c) <= 'z'))
#define ISUPPER(c) (('A' <= (c) && (c) <= 'I') \
	|| ('J' <= (c) && (c) <='R') \
	|| ('S' <= (c) && (c) <= 'Z'))
#define TOUPPER(c) (ISLOWER(c) ? ((c) | 0x40) : (c))
#endif
#define ISDIGIT(c) ('0' <= (c) && (c) <= '9')
#define ISALPHA(c) (ISLOWER(c)||ISUPPER(c))
#define ISALNUM(c) (ISALPHA(c)||ISDIGIT(c))

#endif
