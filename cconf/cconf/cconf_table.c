/*
 * $Id$
 */

#define CCONF_C
#include "cconf.h"

/* table entry */
typedef struct {
	char           *key;
	void           *data;
	void           *upper;
	void           *lower;
}               table_t;

/* all functions are tolerant to null values */

#ifdef __STDC__

static table_t *i_table_alloc(void);	/* allocate new table entry, set all
					 * fields to zero */
static int      i_table_compare(const table_t * t, const char *key);	/* compare t->key to key */

static void    *i_table_get(table_t * t, const char *key);	/* get entry from root
								 * table t */
static void     i_table_put(table_t * t, const char *key, const void *data);	/* put entry to root
										 * table t */

static void     i_table_iterate(table_t * t, cconf_table_iterator iterator, void *buf);

static char    *i_cconf_define_sym(const char *name);

static char    *i_strdup(const char *s);

#else				/* __STDC__ */

static table_t *i_table_alloc();
static int      i_table_compare();

static void    *i_table_get();
static void     i_table_put();

static void     i_table_iterate();

static char    *i_cconf_define_sym();

static char    *i_strdup();

#endif				/* !__STDC__ */

/* root entry */
static table_t  i_table_root;


#ifdef __STDC__
const void     *
cconf_table_get(const char *name, const char *key)
#else
void           *
cconf_table_get(name, key)
	char           *name;
	char           *key;
#endif
{
	table_t        *t;

	if ((t = i_table_get(&i_table_root, name)) == NULL) {
		return NULL;
	}
	if ((t = i_table_get(t->data, key)) == NULL) {
		return NULL;
	} else {
		return t->data;
	}
}

#ifdef __STDC__
void
cconf_table_put(const char *name, const char *key, const void *data)
#else
void
cconf_table_put(name, key, data)
	char           *name;
	char           *key;
	void           *data;
#endif
{
	table_t        *t;

	if ((t = i_table_get(&i_table_root, name)) == NULL) {
		t = i_table_alloc();
		i_table_put(&i_table_root, name, t);
		t = i_table_get(&i_table_root, name);
	}
	i_table_put(t->data, key, data);
}

#ifdef __STDC__
const char     *
cconf_table_string_get(const char *name, const char *key)
#else
char           *
cconf_table_string_get(name, key)
	char           *name;
	char           *key;
#endif
{
	CONST char     *c;

	c = cconf_table_get(name, key);
	return c ? c : "";
}

#ifdef __STDC__
void
cconf_table_string_put(const char *name, const char *key, const char *data)
#else
void
cconf_table_string_put(name, key, data)
	char           *name;
	char           *key;
	char           *data;
#endif
{
	CONST char     *c;
	if ((c = cconf_table_get(name, key)) == NULL) {
		cconf_table_put(name, key, i_strdup(data));
	} else {
		if (c != NULL) {
			free((void *) c);
		}
		cconf_table_put(name, key, i_strdup(data));
	}
}

#ifdef __STDC__
void
cconf_table_string_put_def(const char *name, const char *key, const char *data, const char *altdata)
#else
void
cconf_table_string_put_def(name, key, data, altdata)
	char           *name;
	char           *key;
	char           *data;
	char           *altdata;
#endif
{
	if (data != NULL) {
		cconf_table_string_put(name, key, data);
	} else {
		cconf_table_string_put(name, key, altdata);
	}
}

#ifdef __STDC__
void
cconf_table_string_put_if(const char *name, const char *key, const char *data)
#else
void
cconf_table_string_put_if(name, key, data)
	char           *name;
	char           *key;
	char           *data;
#endif
{
	if (data != NULL) {
		cconf_table_string_put(name, key, data);
	}
}

#ifdef __STDC__
void
cconf_table_iterate(const char *name, cconf_table_iterator iterator, void *buf)
#else
void
cconf_table_iterate(name, iterator, buf)
	char           *name;
	cconf_table_iterator iterator;
	void           *buf;
#endif
{
	table_t        *t;

	if ((t = i_table_get(&i_table_root, name)) == NULL) {
		return;
	}
	i_table_iterate(t->data, iterator, buf);
}

#ifdef __STDC__
void
cconf_define(const char *symbol, const char *value)
#else
void
cconf_define(symbol, value)
	char           *symbol;
	char           *value;
#endif
{
	char           *b;
	if ((symbol == NULL) || (symbol[0] == 0)) {
		/* ignore */
		return;
	}
	b = i_cconf_define_sym(symbol);
	if (value != NULL) {
		cconf_log(LOG_DEBUG, "cconf_define(\"%s\", \"%s\")", b, value);
		cconf_table_put(CCONF_DEF, b, i_strdup(value));
	} else {
		cconf_log(LOG_DEBUG, "cconf_define(\"%s\", <null>)", b);
		cconf_table_put(CCONF_DEF, b, NULL);
	}
	free(b);
}

#ifdef __STDC__
void
cconf_define_have(const char *what, int yes)
#else
void
cconf_define_have(what, yes)
	char           *what;
	int             yes;
#endif
{
	char            buf[256];
	if (what == NULL) {
		/* ignore */
		return;
	}
	sprintf(buf, "HAVE_%s", what);
	cconf_define(buf, yes ? "1" : NULL);
}

#ifdef __STDC__
int
cconf_defined(const char *symbol, char **value)
#else
int
cconf_defined(symbol, value)
	char           *symbol;
	char          **value;
#endif
{
	table_t        *t;
	char           *b;

	if (symbol == NULL) {
		return 0;
	}
	if ((t = i_table_get(&i_table_root, CCONF_DEF)) == NULL) {
		return 0;
	}
	b = i_cconf_define_sym(symbol);
	t = i_table_get(t->data, b);
	free(b);
	if (t == NULL) {
		return 0;
	}
	if (value != NULL) {
		*value = t->data;
	}
	return 1;
}

#ifdef __STDC__
int
cconf_defined_have(const char *what, int *result)
#else
int
cconf_defined_have(what, result)
	char           *what;
	int            *result;
#endif
{
	char            buf[256];
	char           *b;
	sprintf(buf, "HAVE_%s", what);
	if (cconf_defined(buf, &b)) {
		if (result != NULL) {
			*result = (b != NULL) ? 1 : 0;
		}
		return 1;
	} else {
		return 0;
	}
}

#ifdef __STDC__
void
cconf_setenv(const char *symbol, const char *value)
#else
void
cconf_setenv(symbol, value)
	char           *symbol;
	char           *value;
#endif
{
	cconf_table_string_put(CCONF_ENV, symbol, value);
}


#ifdef __STDC__
void
cconf_setenv_app(const char *symbol, const char *value)
#else
void
cconf_setenv_app(symbol, value)
	char           *symbol;
	char           *value;
#endif
{
	char buf[2048];
	sprintf(buf, "%s %s", cconf_table_string_get(CCONF_ENV, symbol), value);
	cconf_table_string_put(CCONF_ENV, symbol, buf);
}

#ifdef __STDC__
int
cconf_set(const char *symbol, char **value)
#else
int
cconf_set(symbol, value)
	char           *symbol;
	char          **value;
#endif
{
	table_t        *t;
	if ((t = i_table_get(&i_table_root, CCONF_ENV)) == NULL) {
		return 0;
	}
	if ((t = i_table_get(t->data, symbol)) == NULL) {
		return 0;
	}
	if (value != NULL) {
		*value = t->data;
	}
	return 1;
}


/*
 * internal functions
 */

static table_t *
i_table_alloc()
{
	table_t        *t;
	t = (table_t *) malloc(sizeof(table_t));
	t->key = NULL;
	t->data = NULL;
	t->upper = NULL;
	t->lower = NULL;
	return t;
}

#ifdef __STDC__
static int
i_table_compare(const table_t * t, const char *key)
#else
static int
i_table_compare(t, key)
	table_t        *t;
	char           *key;
#endif
{
	if ((t == NULL) || (t->key == NULL)) {
		return -1;
	}
	if (key == NULL) {
		return 1;
	}
	return strcmp(t->key, key);
}

#ifdef __STDC__
static void    *
i_table_get(table_t * t, const char *key)
#else
static void    *
i_table_get(t, key)
	table_t        *t;
	char           *key;
#endif
{
	int             i;
	if ((t == NULL) || (key == NULL)) {
		/* not found */
		return NULL;
	}
	while (t != NULL) {
		if ((i = i_table_compare(t, key)) == 0) {
			/* found */
			return t;
		}
		if (i < 0) {
			t = t->lower;
		} else {
			t = t->upper;
		}
	}
	return NULL;
}

#ifdef __STDC__
static void
i_table_put(table_t * t, const char *key, const void *data)
#else
static void
i_table_put(t, key, data)
	table_t        *t;
	char           *key;
	void           *data;
#endif
{
	table_t       **n;
	int             i;
	if ((t == NULL) || (key == NULL)) {
		/* null key or null table not allowed, ignore */
		return;
	}
	while (t != NULL) {
		if ((i = i_table_compare(t, key)) == 0) {
			/* found */
			break;
		}
		if (i < 0) {
			n = (table_t **) & (t->lower);
		} else {
			n = (table_t **) & (t->upper);
		}
		t = *n;
	}
	if (*n == NULL) {
		*n = i_table_alloc();
	}
	t = *n;
	if (t->key == NULL) {
		t->key = i_strdup(key);
	}
	t->data = (void *) data;
}

#ifdef __STDC__
static void
i_table_iterate(table_t * t, cconf_table_iterator iterator, void *buf)
#else
static void
i_table_iterate(t, iterator, buf)
	table_t        *t;
	cconf_table_iterator iterator;
	void           *buf;
#endif
{
	if (t == NULL) {
		return;
	}
	if ((iterator != NULL)) {
		iterator(t->key, t->data, buf);
	}
	i_table_iterate(t->upper, iterator, buf);
	i_table_iterate(t->lower, iterator, buf);
}

#ifdef __STDC__
static char    *
i_cconf_define_sym(const char *name)
#else
static char    *
i_cconf_define_sym(name)
	char           *name;
#endif
{
	char            buf[256];
	int             i;
	strcpy(buf, name);
	if (!ISALPHA(buf[0])) {
		buf[0] = '_';
	}
	for (i = 0; (i < sizeof(buf)) && buf[i]; i++) {
		if (!ISALNUM(buf[i])) {
			buf[i] = '_';
		} else {
			buf[i] = TOUPPER(buf[i]);
		}
	}
	buf[sizeof(buf) - 1] = 0;
	return i_strdup(buf);
}

#ifdef __STDC__
static char    *
i_strdup(const char *s)
#else
static char    *
i_strdup(s)
	char           *s;
#endif
{
	char           *result;
	if (s == NULL) {
		result = (char *) malloc(1);
		result[0] = 0;
	} else {
		result = strdup(s);
	}
	return result;
}
