/*
 * $Id$
 */

#include "cconf.h"

/* internal functions */

#ifdef __STDC__

static int      i_cconf_keyvalue(const char *keyvalue, char *key, char *value);

static void     i_cconf_help(void);
static void     i_cconf_usage(const char *fmt,...);
static void     i_cconf_version(void);
static void     i_cconf_args(int argc, char **argv);

#else				/* __STDC__ */

static int      i_cconf_keyvalue();

static void     i_cconf_help();
static void     i_cconf_usage();
static void     i_cconf_version();
static void     i_cconf_args();

#endif				/* !__STDC__ */

#ifdef __STDC__
int
main(int argc, char *argv[])
#else
int
main(argc, argv)
	int             argc;
	char           *argv[];
#endif
{
	i_cconf_version();
	i_cconf_args(argc, argv);
	cconf_check_cc();
	cconf_user();		/* user defined stuff */
	return 0;
}

static void
i_cconf_help()
{
	msg_console("usage: ./configure [OPTION] ... [VAR=VALUE]...\n");
	msg_console("\nvariables (current settings are shown in brackets)\n\n");
	msg_console(" %-20s %s [%s]\n", "CC", "C compiler command", CC);
	msg_console(" %-20s %s [%s]\n", "CFLAGS", "C compiler flags", CFLAGS);
	msg_console(" %-20s %s [%s]\n", "CPP", "C preprocessor ", CPP);
	msg_console(" %-20s %s [%s]\n", "CXX", "C++ compiler command", CXX);
	msg_console(" %-20s %s [%s]\n", "CXXFLAGS", "C++ compiler flags", CXXFLAGS);
	msg_console(" %-20s %s [%s]\n", "CXXPP", "C++ preprocessor ", CXXPP);
	msg_console(" %-20s %s [%s]\n", "LD", "linker command", LD);
	msg_console(" %-20s %s [%s]\n", "LDFLAGS", "linker flags", LDFLAGS);
	msg_console(" %-20s %s [%s]\n", "LDADD", "libraries", LDADD);
	msg_console("\noptions (defaults are shown in brackets)\n\n");
	msg_console(" %-2s, %-20s %s\n", "-h", "--help", "display this help and exit");
	msg_console(" %-2s, %-20s %s\n", "-V", "--version", "display version and exit");
	msg_console(" %-2s, %-20s %s\n", "-q", "--quiet", "minimal output");
	msg_console("\nInstallation directories\n");
	msg_console(" %-20s %s [%s]\n", "--prefix=PREFIX", "architecture-independent files", cconf_table_string_get(CCONF_SET, "prefix"));
	msg_console(" %-20s %s [%s]\n", "--exec-prefix=EPREFIX", "architecture-dependent files", cconf_table_string_get(CCONF_SET, "exec-prefix"));
	msg_console(" %-20s %s [%s]\n", "--bindir=DIR", "user executables", cconf_table_string_get(CCONF_SET, "bindir"));
	msg_console(" %-20s %s [%s]\n", "--sbindir=DIR", "admin executables", cconf_table_string_get(CCONF_SET, "sbindir"));
	msg_console(" %-20s %s [%s]\n", "--libexecdir=DIR", "program executables", cconf_table_string_get(CCONF_SET, "libexecdir"));
	msg_console(" %-20s %s [%s]\n", "--datadir=DIR", "r/o architecture-independent data", cconf_table_string_get(CCONF_SET, "datadir"));
	msg_console(" %-20s %s [%s]\n", "--sysconfdir=DIR", "r/o single-machine data", cconf_table_string_get(CCONF_SET, "sysconfdir"));
	msg_console(" %-20s %s [%s]\n", "--localstatedir=DIR", "modifiable single-machine data", cconf_table_string_get(CCONF_SET, "localstatedir"));
	msg_console(" %-20s %s [%s]\n", "--libdir=DIR", "object code libraries", cconf_table_string_get(CCONF_SET, "libdir"));
	msg_console(" %-20s %s [%s]\n", "--includedir=DIR", "C header files", cconf_table_string_get(CCONF_SET, "includedir"));
	msg_console(" %-20s %s [%s]\n", "--infodir=DIR", "info documentation", cconf_table_string_get(CCONF_SET, "infodir"));
	msg_console(" %-20s %s [%s]\n", "--mandir=DIR", "man documentation", cconf_table_string_get(CCONF_SET, "mandir"));
	msg_console(" %-20s %s [%s]\n", "--program-prefix=PREFIX", "prepend PREFIX to program names", cconf_table_string_get(CCONF_SET, "program-prefix"));
	msg_console(" %-20s %s [%s]\n", "--program-suffix=SUFFIX", "prepend SUFFIX to program names", cconf_table_string_get(CCONF_SET, "program-suffix"));
	msg_console(" %-20s %s [%s]\n", "--program-transform-name=PROGRAM", "run sed PROGRAM on program names", cconf_table_string_get(CCONF_SET, "program-transform-name"));
	cconf_user_help();
	exit(1);
}

#ifdef __STDC__
static void
i_cconf_usage(const char *fmt,...)
#else
static void
i_cconf_usage(fmt, va_alist)
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	va_list         ap;
	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);
	VA_END(ap);
	msg_console(buf);
	msg_console("usage: ./configure [OPTION] ... [VAR=VALUE]...\n");
	msg_console(" %-2s, %-20s %s\n", "-h", "--help", "display help and exit");
	exit(1);
}

static void
i_cconf_version()
{
	cconf_log(LOG_INFO, "cconf version %s\n", CCONF_VERSION);
}

#ifdef __STDC__
static int
i_cconf_keyvalue(const char *keyvalue, char *key, char *value)
#else
static int
i_cconf_keyvalue(keyvalue, key, value)
	char           *keyvalue;
	char           *key;
	char           *value;
#endif
{
	int             i, k, v;
	for (i = 0, k = 0; keyvalue[i] && (keyvalue[i] != '='); i++) {
		key[k++] = keyvalue[i];
	}
	key[k] = 0;

	if (keyvalue[i] == '=') {
		i++;
	} else {
		return 0;
	}

	for (v = 0; keyvalue[i]; i++) {
		value[v++] = keyvalue[i];
	}
	value[v] = 0;
	return 1;
}

#ifdef __STDC__
static void
i_cconf_args(int argc, char **argv)
#else
static void
i_cconf_args(argc, argv)
	int             argc;
	char           *argv[];
#endif
{
	char            key[128];
	char            value[128];
	int             i;

	cconf_table_string_put(CCONF_SET, "prefix", "/usr/local");
	cconf_table_string_put(CCONF_SET, "exec-prefix", "/usr/local");
	cconf_table_string_put(CCONF_SET, "bindir", "bin");
	cconf_table_string_put(CCONF_SET, "sbindir", "sbin");
	cconf_table_string_put(CCONF_SET, "libexecdir", "libexec");
	cconf_table_string_put(CCONF_SET, "datadir", "share");
	cconf_table_string_put(CCONF_SET, "sysconfdir", "etc");
	cconf_table_string_put(CCONF_SET, "sharedstatedir", "com");
	cconf_table_string_put(CCONF_SET, "localstatedir", "var");
	cconf_table_string_put(CCONF_SET, "libdir", "lib");
	cconf_table_string_put(CCONF_SET, "includedir", "include");
	cconf_table_string_put(CCONF_SET, "infodir", "info");
	cconf_table_string_put(CCONF_SET, "mandir", "man");

	cconf_table_string_put(CCONF_SET, "program-prefix", "");
	cconf_table_string_put(CCONF_SET, "program-suffix", "");
	cconf_table_string_put(CCONF_SET, "program-transform-name", "");

	cconf_user_init();

	for (i = 1; i < argc; i++) {
		if (!(strcmp(argv[i], "-h") && strcmp(argv[i], "--help"))) {
			/* help */
			i_cconf_help();
			/* notreached */
		} else if (!(strcmp(argv[i], "-V") && strcmp(argv[i], "--version"))) {
			/* version */

			/**
			 * version has already been displayed, so we just exit
			 */
			//i_cconf_version();

			exit(1);
		} else if (!(strcmp(argv[i], "-q") && strcmp(argv[i], "--quiet") && strcmp(argv[i], "--silent"))) {
			log_level(LOG_ERROR);
		} else if ((argv[i][0] == '-') && (argv[i][1] == '-')) {
			if (i_cconf_keyvalue(argv[i] + 2, key, value) && cconf_table_get(CCONF_SET, key)) {
				cconf_table_string_put(CCONF_SET, key, value);
			} else {
				i_cconf_usage("invalid parameter: %s\n", key);
				/* notreached */
			}
		} else {
			if (i_cconf_keyvalue(argv[i], key, value)) {
				cconf_table_string_put(CCONF_ENV, key, value);
			} else {
				i_cconf_usage("invalid parameter: %s\n", argv[i]);
				/* notreached */
			}
		}
	}

	if (!cconf_set("CC", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "CC", getenv("CC"), "cc");
	}
	if (!cconf_set("CFLAGS", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "CFLAGS", getenv("CFLAGS"), "");
	}
	if (!cconf_set("CPP", NULL)) {
		sprintf(key, "%s -E", CC);
		cconf_table_string_put_def(CCONF_ENV, "CPP", getenv("CPP"), key);
	}
	if (!cconf_set("CXX", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "CXX", getenv("CXX"), CC);
	}
	if (!cconf_set("CXXFLAGS", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "CXXFLAGS", getenv("CXXFLAGS"), CFLAGS);
	}
	if (!cconf_set("CXXCPP", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "CXXPP", getenv("CXXPP"), CPP);
	}
	if (!cconf_set("CPPFLAGS", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "CPPFLAGS", getenv("CPPFLAGS"), CFLAGS);
	}
	if (!cconf_set("LD", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "LD", getenv("LD"), CC);
	}
	if (!cconf_set("LDFLAGS", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "LDFLAGS", getenv("LDFLAGS"), CFLAGS);
	}
	if (!cconf_set("LDADD", NULL)) {
		cconf_table_string_put_def(CCONF_ENV, "LDADD", getenv("LDADD"), "");
	}
}
