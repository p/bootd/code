/*
 * $Id$
 */

#define CCONF_LOG_C
#include "cconf.h"

#ifdef __STDC__

static char    *i_strerror(void);
static void     i_log_file(int level, const char *msg);

#else				/* __STDC__ */

static char    *i_strerror();
static void     i_log_file();

#endif				/* !__STDC__ */

static int      i_log_file_indent = 0;
static int      i_log_level = LOG_INFO;

#ifdef __STDC__
void
cconf_log(int level, const char *fmt,...)
#else
void
cconf_log(level, fmt, va_alist)
	int             level;
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	char           *c;
	int             i, fd;
	va_list         ap;

	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);	/* not using vsnprintf for portability */
	VA_END(ap);

	/* make sure it's terminated, and add strerror() if desired */
	if (level & LOG_ERRNO) {
		buf[sizeof(buf) - 100] = 0;
		strcat(buf, ": ");
		strcat(buf, i_strerror());
		strcat(buf, "\n");
	} else {
		buf[sizeof(buf) - 1] = 0;
	}

	c = buf;
	for (i = 0; i < sizeof(buf); i++) {
		if (buf[i] == 0) {
			/* the end */
			if (i == 0) {
				return;
			}
			i_log_file(level, c);
			break;
		} else if (buf[i] == '\n') {
			buf[i] = 0;
			i_log_file(level, c);
			c = buf + i + 1;
			buf[i] = '\n';
		}
	}

	level &= LOG_LEVEL;
	if (level < i_log_level) {
		return;
	}
	if (level <= LOG_INFO) {
		/* stdout */
		fd = 1;		/* stdout */
	} else {
		fd = 2;		/* stderr */
	}

	if (write(fd, buf, i) != i) {
		/*
		 * error, terminate. this 'should not happen' (tm) if it does
		 * anyway, the printf will probably fail, too
		 */
		printf("write(\"%s\", buf, %d)", CCONF_LOGFILE, i);
		exit(1);
	}
	if (level == LOG_ERROR) {
		exit(1);
	}
}

#ifdef __STDC__
void
msg_quest(const char *fmt,...)
#else
void
msg_quest(fmt, va_alist)
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	va_list         ap;

	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);
	VA_END(ap);
	cconf_log(LOG_INFO, "checking %s... ", buf);
}

#ifdef __STDC__
void
msg_quest_checking_for(const char *fmt,...)
#else
void
msg_quest_checking_for(fmt, va_alist)
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	va_list         ap;

	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);
	VA_END(ap);
	cconf_log(LOG_INFO, "checking for %s... ", buf);
}

#ifdef __STDC__
void
msg_quest_checking_if(const char *fmt,...)
#else
void
msg_quest_checking_if(fmt, va_alist)
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	va_list         ap;

	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);
	VA_END(ap);
	cconf_log(LOG_INFO, "checking if %s... ", buf);
}

void
msg_cached()
{
	msg_console("(cached) ");
}

#ifdef __STDC__
void
msg_console(const char *fmt,...)
#else
void
msg_console(fmt, va_alist)
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	va_list         ap;

	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);
	VA_END(ap);
	cconf_log(LOG_INFO | LOG_CONSOLE, buf);
}

#ifdef __STDC__
void
msg_ans(const char *fmt,...)
#else
void
msg_ans(fmt, va_alist)
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	va_list         ap;

	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);
	VA_END(ap);
	cconf_log(LOG_INFO, "%s\n", buf);
}

#ifdef __STDC__
void
msg_ans_yesno(int yesno)
#else
void
msg_ans_yesno(yesno)
	int             yesno;
#endif
{
	cconf_log(LOG_INFO, yesno ? "yes\n" : "no\n");
}

#ifdef __STDC__
void
msg_ans_found(int found)
#else
void
msg_ans_found(found)
	int             found;
#endif
{
	cconf_log(LOG_INFO, found ? "found\n" : "not found\n");
}

#ifdef __STDC__
void
log_func_start(const char *name, const char *fmt,...)
#else
void
log_func_start(name, fmt, va_alist)
	char           *name;
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	va_list         ap;
	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);
	VA_END(ap);
	cconf_log(LOG_DEBUG, "<%s %s>", name, buf);
	i_log_file_indent += 2;
}

#ifdef __STDC__
void
log_func_end(const char *name, const char *fmt,...)
#else
void
log_func_end(name, fmt, va_alist)
	char           *name;
	char           *fmt;
va_dcl
#endif
{
	char            buf[2048];
	va_list         ap;
	i_log_file_indent -= 2;
	VA_START(ap, fmt);
	vsprintf(buf, fmt, ap);
	VA_END(ap);
	cconf_log(LOG_DEBUG, "</%s %s>", name, buf);
}

#ifdef __STDC__
void
log_notfound(int level, int what, const char *fmt, ...)
#else
void
log_notfound(level, what, va_alist)
	int level;
	int what;
	char *fmt;
	va_dcl
#endif
{
	char buf1[2048];
	char buf2[2048];
	va_list ap;
	char *cl, *cd;

	switch (level) {
		case LOG_DEBUG:
			cl = "DEBUG:\n%s\n\n";
			break;
		case LOG_INFO:
			cl = "INFO:\n%s\n  building will continue\n\n";
			break;
		case LOG_WARN:
			cl = "WARNING:\n%s\n  building may fail, continuing anyway\n\n";
			break;
		case LOG_ERROR:
			cl = "ERROR:\n%s\n  cannot continue\n\n";
			break;
		default:
			cl ="%s\n";
			break;
	}

	switch (what) {
		case NOTFOUND_HEADER:
			cd = "header file %s has not been found, please add -I<dir> to CFLAGS\n  (<dir> is the directory containing this header file)";
			break;
		case NOTFOUND_FUNCTION:
			cd = "function %s has not been found, maybe you need to add -l<lib> to LDADD\n  (<lib> is the library containing this function)";
			break;
		case NOTFOUND_LIBRARY:
			cd = "library %s has not been found, please add -L<dir> to LDFLAGS\n  (dir> is the directory containing the library)";
			break;
		case NOTFOUND_TYPE:
			cd = "type %s has not been found";
			break;
		case NOTFOUND_PROG:
			cd = "program %s has not been found, please add it's path to $PATH";
			break;
		case NOTFOUND_UNSPEC:
			cd = "%s";
			break;
		default:
			cd = "%s has not been found";
			break;
	}
	VA_START(ap, fmt);
	vsprintf(buf1, fmt, ap);
	VA_END(ap);

	sprintf(buf2, cd, buf1);
	sprintf(buf1, cl, buf2);
	cconf_log(level, buf1);
}

#ifdef __STDC__
void
log_level(int level)
#else
void 
log_level(level)
	int             level;
#endif
{
	i_log_level = level;
}

/*
 * internal functions
 */

static char    *
i_strerror()
{
	/* PORTABILITY? */
	return strerror(errno);
}

#ifdef __STDC__
static void
i_log_file(int level, const char *msg)
#else
static void
i_log_file(level, msg)
	int             level;
	char           *msg;
#endif
{
	int             fd;
	int             i;
	CONST char     *slevel;

	if (level & LOG_CONSOLE) {
		return;
	}
	level &= LOG_LEVEL;
	if ((i = strlen(msg)) == 0) {
		return;
	}
	if ((fd = open(CCONF_LOGFILE, O_CREAT | O_APPEND | O_WRONLY, 0666)) == -1) {
		printf("open(\"%s\", CREAT|APPEND|WRONLY): %s\n", CCONF_LOGFILE, i_strerror());
		exit(1);
	}
	switch (level) {
	case LOG_DEBUG:
		slevel = "DEBUG ";
		break;
	case LOG_INFO:
		slevel = "INFO  ";
		break;
	case LOG_WARN:
		slevel = "WARN  ";
		break;
	case LOG_ERROR:
		slevel = "ERROR ";
		break;
	default:
		slevel = "      ";
		break;
	}
	if (write(fd, slevel, strlen(slevel)) != strlen(slevel)) {
		printf("write(\"%s\", buf, %d): %s\n", CCONF_LOGFILE, strlen(slevel), i_strerror());
		exit(1);
	}
	if (i_log_file_indent > 0) {
		if (i_log_file_indent > 40) {
			i_log_file_indent = 40;
		}
		if (write(fd, "                                        ",
			  i_log_file_indent) != i_log_file_indent) {
			printf("write(\"%s\", buf, %d): %s\n", CCONF_LOGFILE, i_log_file_indent, i_strerror());
			exit(1);
		}
	}
	if (write(fd, msg, i) != i) {
		printf("write(\"%s\", buf, %d): %s\n", CCONF_LOGFILE, i, i_strerror());
		exit(1);
	}
	if (write(fd, "\n", 1) != 1) {
		printf("write(\"%s\", buf, %d): %s\n", CCONF_LOGFILE, 1, i_strerror());
		exit(1);
	}
	close(fd);
}
