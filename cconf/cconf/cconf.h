/*
 * $Id$
 */

#ifndef CCONF_H
#define CCONF_H

#include "cconf_i.h"
#define CCONF_VERSION "0.1"
#define CCONF_WORKDIR "cconf_work"
#define CCONF_LOGFILE "cconf.log"
#define CCONF_CONFDEFS_H "confdefs.h"

#ifdef __STDC__

/**
 * user defined actions
 * this function should be placed in cconf_user.c and should contain
 * all the calls to cconf_check_XXX and cconf_write_XXX except for
 * cconf_check_cc() which is always done before calling cconf_user().
 *
 * it will be called from main after initialization with no
 * parameters.
 * example:
 *	#include "cconf.h"
 *	void cconf_user() {
 *		cconf_check_header("foo.h");
 *		cconf_check_header("bar.h");
 *		cconf_check_headers_ansi();
 *		cconf_check_func("strerror", NULL);
 *		if (!cconf_check_func("kvm_open", "-lkvm", NULL)) {
 *			cconf_log(LOG_ERROR, "I really need kvm_open!!!\n");
 *			// notreached
 *		}
 *		if (!cconf_check_type("struct tm")) {
 *			cconf_log(LOG_ERROR, "I really need struct tm!!!\n");
 *			// notreached
 *		}
 *		cconf_check_type_def("size_t"), "unsigned int");
 *		cconf_write_config_h("config.h");
 *		cconf_write_env("Makefile.inc");
 */
void            cconf_user(void);
#else
void            cconf_user();
#endif


/*
 * cconf_log
 */

#define LOG_DEBUG	0	/* to logfile only */
#define LOG_INFO	1	/* to logfile and stdout */
#define LOG_WARN	2	/* to logfile and stderr */
#define LOG_ERROR	3	/* to logfile and stderr, terminate */
#define	LOG_LEVEL	0x000f	/* mask for log level */
#define LOG_ERRNO	0x1000	/* append error message (strerror(errno)) */
#define LOG_CONSOLE	0x2000	/* to console only */

#ifdef __STDC__

/**
 * write a log message
 * This function writes a message to either the logfile, stdout,
 * or stderr, depending on the value of level.
 *
 * the following levels are defined
 *
 *	LOG_DEBUG    log to file only
 *	LOG_INFO     log to file and stdout
 *	LOG_WARN     log to file and stderr
 *	LOG_ERROR    log to file and stderr, terminate
 *	LOG_CONSOLE  log to console only
 *
 * the following flags are defined (arith. ored to the level)
 *
 *	LOG_ERRNO    append error message (strerror(errno))
 *
 * example:
 *	cconf_log(LOG_ERROR|LOG_ERRNO,
 *		"open(%s, "O_RDWR|O_CREAT|O_APPEND", filename);
 *
 * will write a message to the logfile and stderr, with the apropriate
 * system error message from errno appended, and terminate

 *	cconf_log(LOG_INFO|LOG_CONSOLE, "\n");
 *
 * will write a newline to stdout only. this is preferred over
 * printf as it allows globally turning off console output
 */
void
cconf_log(int level,		/* specifics, see LOG_XXX */
	  const char *fmt,	/* format string as in printf */
	  ...			/* arguments to printf */
);

/**
 * checking XYZ...
 * this essentially calls
 *	cconf_log(LOG_INFO, "checking " + fmt + "...")
 */
void
msg_quest(const char *fmt,	/* format string as in printf */
	  ...			/* arguments to printf */
);

/**
 * checking for XYZ...
 * this essentially calls
 *	cconf_log(LOG_INFO, "checking for " + fmt+ "...")
 */
void
msg_quest_checking_for(const char *fmt,	/* format string as in printf */
		       ... /* arguments to printf */ );

/**
 * checking if XYZ...
 * this essentially calls
 *	cconf_log(LOG_INFO, "checking if " + fmt+ "...")
 */
void
msg_quest_checking_if(const char *fmt,	/* format string as in printf */
		      ...	/* arguments to printf */
);

/**
 * (cached)
 * this essentially calls
 *	msg_console("(cached)")
 */
void            msg_cached(void);

/**
 * write message to console only
 * this essentially calls
 *	cconf_log(LOG_INFO|LOG_CONSOLE, fmt)
 */
void
msg_console(const char *fmt,	/* format string as in printf */
	    ...			/* arguments to printf */
);

/**
 * XYZ<newline>
 * this essentially calls
 *	cconf_log(LOG_INFO, fmt + "\n")
 */
void
msg_ans(const char *fmt,	/* format string as in printf */
	...			/* arguments to printf */
);

/**
 * "yes" or "no" <newline>
 * this essentially calls
 *	msg_ans(yesno?"yes":"no")
 */
void            msg_ans_yesno(int yesno /* truth value */ );

/**
 * "found" or "not found" <newline>
 * this essentially calls
 *	msg_ans(found?"found":"not found")
 */
void            msg_ans_found(int found /* truth value */ );

/**
 * open indentation level in logfile
 * to make the logfile more readable, logging to file maintains an
 * indentation level to make it easy to see what belongs together.
 *
 * use fmt to add a description or log the arguments to the function.
 * example:
 *	void testfunc(char *filename)
 *	{
 *		int result;
 *		log_func_start("testfunc", "filename=%s", filename);
 *		result = random() % 2;
 *		log_func_end("testfunc", "result=%d", result);
 *	}
 */
void
log_func_start(const char *name,/* name of the function */
	       const char *fmt,	/* format string as in printf */
	       ... /* arguments to printf */ );

/**
 * close indentation level in logfile
 * to make the logfile more readable, logging to file maintains an
 * indentation level to make it easy to see what belongs together.
 *
 * use fmt to add a description or log the result of the function.
 * example:
 * see log_func_start for an example
 */
void
log_func_end(const char *name,	/* name of the function */
	     const char *fmt,	/* format string as in printf */
	     ... /* arguments to printf */ );

/**
 * warning/error: xyz has not been found
 * print an error/warning that something has not been found.
 * together with a short description how to fix
 *
 * the following 'somethings' are defined
 *
 *	NOTFOUND_HEADER		header file
 *	NOTFOUND_FUNCTION	function
 *	NOTFOUND_LIBRARY	library
 *	NOTFOUND_TYPE		type
 *	NOTFOUND_PROG		program
 *	NOTFOUND_UNSPEC		unspecified, no generic text will be added
 */
#define NOTFOUND_HEADER	1
#define NOTFOUND_FUNCTION	2
#define NOTFOUND_LIBRARY	3
#define NOTFOUND_TYPE		4
#define NOTFOUND_PROG		5
#define NOTFOUND_UNSPEC	0

void
log_notfound(int level,	/* log level for cconf_log */
	int what,	/* what */
	  const char *fmt,	/* format string as in printf */
	  ...			/* arguments to printf */
	  );

/**
 * set loglevel to console
 * don't log anything to console below the given level. anything
 * below will be logged to file only. default value is LOG_INFO.
 */
void            log_level(int level /* minimal loglevel */ );

#else				/* __STDC__ */

void            cconf_log();
void            msg_quest();
void            msg_quest_checking_for();
void            msg_quest_checking_if();
void            msg_cached();
void            msg_console();
void            msg_ans();
void            msg_ans_yesno();
void            msg_ans_found();
void            log_func_start();
void            log_func_end();
void            log_level();

#endif				/* !__STDC__ */


/*
 * cconf_table
 */

#define CCONF_ENV	"env"
#define CCONF_DEF	"def"
#define CCONF_SET	"set"

#define CC	cconf_table_string_get(CCONF_ENV, "CC")
#define CFLAGS	cconf_table_string_get(CCONF_ENV, "CFLAGS")
#define CPP	cconf_table_string_get(CCONF_ENV, "CPP")
#define CXX	cconf_table_string_get(CCONF_ENV, "CXX")
#define CXXFLAGS	cconf_table_string_get(CCONF_ENV, "CXXFLAGS")
#define CXXPP	cconf_table_string_get(CCONF_ENV, "CXXPP")
#define LD	cconf_table_string_get(CCONF_ENV, "LD")
#define LDFLAGS	cconf_table_string_get(CCONF_ENV, "LDFLAGS")
#define LDADD	cconf_table_string_get(CCONF_ENV, "LDADD")


#ifdef __STDC__

typedef void    (*cconf_table_iterator) (const char *key, const void *value, void *buf);

/**
 * get an entry from a table
 * get entry with key "key" from table named "name"
 *
 * do not modify the contents of the pointer returned (hence the
 * "const"). instead, use cconf_table_put to put a new value.
 *
 * currently, the following table names are defined
 *
 *	CCONF_ENV ("env")	environment variables
 *	CCONF_DEF ("def")	C defines
 *	CCONF_SET ("set")	settings
 *
 * returns: pointer to data
 */
const void     *
cconf_table_get(const char *name,	/* name of the table */
		const char *key /* key for the entry */ );

/**
 * put an entry into a table
 * put entry with key "key" into table named "name"
 *
 * bugs:
 * because we cannot know the size of the entry, memory allocation
 * must be done by the caller. cconf_table_put stores nothing but a
 * pointer to the data, and it will happily overwrite any previous
 * value, causing a memory leak.
 *
 */
void
cconf_table_put(const char *name,	/* name of the table */
		const char *key,/* key for the entry */
		const void *data /* pointer to data */ );

/**
 * get a string from a table
 * get string with key "key" from table named "name". If the entry
 * is 'NULL', the empty string ("") will be returned, thus it is
 * 'failsafe' in the sense that it can be used as an argument to a
 * function without checking for NULL.
 *
 * the following are convenient shortcuts for often used entries
 *
 *	CC 		the C compiler
 *	CFLAGS		flags to the C compiler
 *	CPP		the C preprocessor
 *	CXX		the C++ compiler
 *	CXXFLAGS	flags to the C++ compiler
 *	CXXPP		the C++ preprocessor
 *	LD		the linker
 *	LDFLAGS		flags to the linker
 *	LDADD		additional libraries for linking
 * bugs:
 * there is no guarantee that the entry returned is in fact a string,
 * it will use whatever data is stored there, treating it as a string.
 */
const char     *
cconf_table_string_get(const char *name,	/* name of the table */
		       const char *key /* key for the entry */ );

/**
 * put a string into a table
 * put string with key "key" into table named "name". This function
 * uses strdup(), so the data pointed to by 'data' is not required to
 * be persistent. It also handles the case where an entry with the
 * same key also exists by free()ing it first. This implies that
 * whatever is already there must have been allocated by malloc().
 */
void
cconf_table_string_put(const char *name,	/* name of the table */
		       const char *key,	/* key for the entry */
		       const char *data /* pointer to string */ );

/**
 * put a string into a table with default value
 * put string with key "key" into table named "name". If data is
 * NULL, use default value. Otherwise behaves exactly like
 * cconf_table_string_put
 */
void
cconf_table_string_put_def(const char *name,	/* name of the table */
			   const char *key,	/* key for the entry */
			   const char *data,	/* pointer to string */
			   const char *defdata	/* default string to use if
						 * data is NULL */
);

/**
 * put a string into a table if it is not NULL
 * put string with key "key" into table named "name". If data is
 * NULL, do nothing. Otherwise behaves exactly like cconf_table_string_put
 */
void
cconf_table_string_put_if(const char *name,	/* name of the table */
			  const char *key,	/* key for the entry */
			  const char *data	/* pointer to string */
);

/**
 * call iterator for every entry in table
 * goes through the table named 'name' and calls iterator for every entry.
 *
 * the iterator is defined as
 *	typedef void    (*cconf_table_iterator) (
 *	 			const char *key, 	// key of the entry
 *				const void *value,	// value of the entry
 *				void *buf		// internal state
 *			);
 *
 * buf is a pointer to some internal state that can be used freely
 * to maintain some internal state.
 * example:
 *	void my_iterator(const char *key, const void *value, void *buf)
 *	{
 *		if (value!=NULL) {
 *			*(int*)buf+=*(int*)value;
 * 		}
 *	}
 *
 *	int sum;
 *	cconf_table_iterate("values", my_iterator, &sum);
 *
 * this will compute the sum of all values in table "values"
 */
void
cconf_table_iterate(const char *name,	/* name of the table */
		    cconf_table_iterator iterator,	/* iterator function */
		    void *buf	/* internal state */
);

/**
 * define XYZ...
 * symbol is mangled so that it consists only of upper case letters,
 * numbers and underscores (and does not start with a number), and is
 * then put into the table CCONF_DEF. If value is not NULL, it is
 * strdup()ed first. A value of NULL corresponds to undefined.
 */
void
cconf_define(const char *symbol,/* symbol name */
	     const char *value /* symbol value */ );

/**
 * define HAVE_XYZ...
 * the same as cconf_define(...), but "HAVE_" is prepended to symbol,
 * and data is either NULL (yes==0) or "1" (yes==1).
 */
void
cconf_define_have(const char *what,	/* symbol name */
		  int yes /* symbol value */ );

/**
 * return 1 if symbol is defined
 * this function returns 1 if symbol 'symbol' is defined in table
 * CCONF_DEF (even if the value is NULL, corresponding to undefined),
 * and puts it's value into *value (unless value is NULL). If 'symbol'
 * is not defined in table CCONF_DEF, *value is left unchanged.
 *
 * this does not correspond to the C/C++ #ifdef directive, but is
 * intended to check whether it is already known whether symbol is
 * defined or not.
 * example:
 *	char *c;
 *	if (cconf_defined("ansi_headers", &c))
 *		if (c!=NULL) {
 *			printf("Yes, we have ansi headers\n");
 *		} else {
 *			printf("No, we don't have ansi headers\n");
 *		}
 *	} else {
 *		printf("we don't yet know whether we have ANSI headers\n");
 *	}
 * returns:
 * 1 if there is an entry named 'symbol' in table CCONF_DEF, 0
 * otherwise.
 *
 * if either symbol is NULL, 0 is returned and value is left
 * unchanged.
 */
int
cconf_defined(const char *symbol,	/* symbol name */
	      char **value	/* holder for value */
);

/**
 * return 1 if HAVE_symbol is defined
 * the same as cconf_defined(...), but prepend HAVE_ to symbol.
 * value should point to an int, and will contain either 0 or 1,
 * depending on whether the entry contains NULL or some other value.
 * returns:
 * 1 if there is an entry named HAVE_'symbol' in table CCONF_DEF,
 * 0 otherwise
 */
int
cconf_defined_have(const char *what,	/* symbol name */
		   int *value /* holder for value */ );

/**
 * put symbol in CCONF_ENV
 * symbol is put into table CCONF_ENV using cconf_table_string_put().
 */
void
cconf_setenv(const char *symbol,/* symbol name */
	     const char *value	/* value */
);

/**
 * append to symbol in CCONF_ENV
 * append (like +=) to value of symbol 'symbol' in table CCONF_ENV
 */
void
cconf_setenv_app(const char *symbol, /* symbol name */
	   const char *value /* value */
);

/**
 * return 1 if symbol is set
 * this function returns 1 if symbol 'symbol' is set in table
 * CCONF_ENV (even if the value is NULL, corresponding to unset),
 * returns:
 * 1 if there is an entry named 'symbol' in table CCONF_ENV, 0
 * otherwise.
 *
 * if either symbol is NULL, 0 is returned and value is left
 * unchanged.
 */
int
cconf_set(const char *symbol,	/* symbol name */
	  char **value /* value */ );

#else				/* __STDC__ */

typedef void    (*cconf_table_iterator) ();

void           *cconf_table_get();
void            cconf_table_put();

char           *cconf_table_string_get();
void            cconf_table_string_put();
void            cconf_table_string_put_def();

void            cconf_table_iterate();

void            cconf_define();
void            cconf_define_have();
int             cconf_defined();
int             cconf_defined_have();

void            cconf_setenv();
int             cconf_set();

#endif				/* !__STDC__ */


/*
 * cconf_func
 */

#ifdef __STDC__

/**
 * write defines to file
 * uses cconf_table_iterate to write all entries in table CCONF_DEF
 * to a file (typically named config.h). only #define and #undef
 * directives are written to that file. no comments, no specific
 * ordering. the file will be overwritten, all errors are fatal.
 */
void            cconf_write_config_h(const char *filename /* filename */ );

/**
 * write environment to file
 * uses cconf_table_iterate to write all entries in table CCONF_ENV
 * to a file in the form var=value. no comments, no specific ordering.
 * the file will be overwritten, all errors are fatal.
 */
void            cconf_write_env(const char *filename /* filename */ );

/**
 * run command
 * use system() to run command. write the contents of stdinbuf to
 * stdin (unless stdinbuf is NULL), and write stdout/stderr to
 * stdoutbuf/stderrbuf (unless they are NULL) up to the length
 * specified by stdoutlen/stderrlen. the actual amount of data is
 * stored in stdoutlen/stderrlen. stdoutbuf and stderrbuf are
 * guaranteed to be 0-terminated, but they may contain 0 values
 * in the middle, if they were output by the program.
 * returns:
 * 1 if the command has been run and terminated with 0 exit value,
 * 0 otherwise.
 * bugs:
 * for portability, no pipes are used, instead, stdin, stdout, and
 * stderr are connected to files. this may cause a serious performance
 * impact.
 * example:
 *	char *stderrbuf;
 *	int stderrlen;
 *	if (cconf_try_run("./myprog", "blabla", 6, NULL, NULL, stderrbuf, &stderrlen))) {
 *		printf("myprog run OK\n");
 *	}
 *	if (strstr(stderrbuf, "BLABLA")) {
 *		printf("myprog output OK\n");
 *	}
 */
int
cconf_try_run(const char *command,	/* command to run including
					 * parameters */
	      char *stdinbuf,	/* data to feed as stdin */
	      int stdinlen,	/* length of stdinbuf */
	      char *stdoutbuf,	/* stdout */
	      int *stdoutlen,	/* maximal/actual length of stdoutbuf */
	      char *stderrbuf,	/* stderr */
	      int *stderrlen	/* maximal/actual length of stderrbuf */
);
/**
 * run the C compiler
 * Put 'srctext' into a file named 'name', and try to compile.
 *
 * the variables "CC" and "CFLAGS" from the table CCONF_ENV are
 * used.
 *
 * no linking is done (use cconf_try_link() for that).
 * returns:
 * 1 if compiled successfully, 0 otherwise.
 * example:
 *	if (!cconf_try_cc("#include "blabla.h"\nint main() { return bar();\n} \n", "foo.c")) {
 *		printf("compiling foo.c failed\n");
 *		exit(1);
 *	}
 *	if (!cconf_try_link("foo", "foo.o", "-lbar", NULL)) {
 *		printf("linking foo.o and libbar.a into foo failed\n");
 *		exit(1);
 *	}
 *	if (!cconf_try_run("./foo", NULL, 0, NULL, NULL, NULL, NULL)) {
 *		printf("./foo returned with non-zero exit status\n");
 *		exit(1);
 *	}
 *	printf("foo compiled, linked, and run OK\n");
 */
int
cconf_try_cc(const char *srctext,	/* C program source text */
	     const char *name	/* file to put it into */
);

/**
 * run the linker
 * try to link the specified files into an executable. The first
 * parameter is the name of the output file, the rest is a NULL
 * terminated list of input object files and libraries.
 *
 * the variables "LD", "LDFLAGS", and "LDADD" from the table
 * CCONF_ENV are used.
 * returns:
 * 1 if linked successfully, 0 otherwise.
 * example:
 * see cconf_try_cc for an example.
 */
int
cconf_try_link(const char *name,/* name of output file */
	       ... /* NULL terminated list of input files */ );

/**
 * run the C preprocessor
 * run the specified text through the C preprocessor, and put the result
 * into stdoutbuf. stdoutlen should contain the maximal length of stdoutbuf,
 * the actual amount of data will be placed in there
 *
 * the variables "CPP" and "CPPFLAGS" from the table CCONF_ENV will
 * be used.
 *
 * 1 if preprocessed successfully, 0 otherwise.
 * example:
 * see cconf_try_cc for an example.
 */
int
cconf_try_cpp(const char *srctext,	/* source text */
	      char *stdoutbuf,	/* output buffer */
	      int *stdoutlen /* maximal/actual length of stdoutbuf */ );

/**
 * check whether the C compiler works
 * runs various tests to make sure the C compiler works as expected,
 * and sets all C compiler variables to default values if they are
 * not already set.
 * returns:
 * 1 if compiler works, exit on error (no sense continuing if
 * compiler does not work)
 */
int             cconf_check_cc(void);

/**
 * check whether header file is available
 * compile a small test program to check whether the specified
 * header file is available on this system, and sets HAVE_'header'
 * accordingly (to either "1" or NULL).
 * returns:
 * 1 if file is available, 0 otherwise
 */
int             cconf_check_header(const char *header /* name of header file */ );

/**
 * check whether a set of header files is available
 * compile a small test program to check for all specified header
 * files in one run. if that fails, try them one by one. for every
 * header file, HAVE_'header' is defined to "1" if available NULL
 * otherwise, and 'desc' is defined to "1" if all of them are available.
 *
 * this has the same effect as calling cconf_check_header() for each file
 * and then calling cconf_define(desc) if they were all available, but
 * if all of them are available, it is much faster.
 * returns:
 * 1 if all header files are available 0 otherwise
 */
int
cconf_check_headers(const char *desc,	/* description */
		    ... /* NULL terminated list of header files */ );

/**
 * check whether ANSI headers are available
 * calls
 *	cconf_check_headers("ANSI headers", "assert.h", "ctype.h",
 *		"errno.h", "float.h", "limits.h", "locale.h", "math.h",
 *		"setjmp.h", "signal.h", "stdarg.h", "stddef.h", "stdio.h",
 *		"stdlib.h", "string.h", "time.h", NULL);
 * returns:
 * 1 if all ANSI header files are available, 0 otherwise.
 */
int             cconf_check_headers_ansi(void);

/**
 * check whether POSIX.1 headers are available
 * calls
 *	cconf_check_headers("POSIX.1 headers", "cpio.h", "dirent.h",
 *		"fcntl.h", "grp.h", "pwd.h", "tar.h", "termios.h", "unistd.h",
 *		"utime.h", "sys/stat.h", "sys/times.h", "sys/types.h", "sys/utsname.h", "sys/wait.h",
 *		NULL);
 * returns:
 * 1 if all POSIX.1 header files are available, 0 otherwise.
 */
int             cconf_check_headers_posix1(void);

/**
 * check whether XPG3 headers are available
 * calls
 *	cconf_check_headers("XPG3 headers", "ftw.h", "langinfo.h", "nl_types.h", "regex.h", "search.h",
 *		"ulimit.h", "sys/ipc.h", "sys/msg.h", "sys/sem.h", NULL);
 * note:
 * this checks only those header files not already tested for in POSIX.1
 * returns:
 * 1 if all XPG3 header files are available, 0 otherwise.
 */
int             cconf_check_headers_xpg3(void);

/**
 * check whether function 'func' is available
 * tries to link a little test program to check whether 'func' is
 * available. first, it tries to compile it without additional
 * libraries, then with each specified library in turn.
 * returns:
 * 1 if 'func' is available, 0 otherwise.
 */
int
cconf_check_func(const char *func,	/* function */
		 ... /* NULL terminated list of libraries */ );

/**
 * check whether type 'type' is available
 * tries to compile a little test program to check wheter 'type'
 * is available. all header files tested for so far are included.
 * returns:
 * 1 if 'type' is available, 0 otherwise.
 */
int             cconf_check_type(const char *type /* type */ );

/**
 * check whether type 'type' is available, typedef to default if not
 * use cconf_check_type to check whether 'type' is available and
 * cconf_define()s it to the supplied default value if not
 */
void 
cconf_check_type_def(const char *type,	/* type */
		     const char *def /* default */ );

/**
 * check for 'basic' standard types
 * checks for [u]int_{8,16,32}_t, size_t, ssize_t, pid_t etc
 */
void            cconf_check_std_types(void);

/**
 * read file
 * reads file named 'name' into buf
 * returns:
 * number of bytes read, 0 on error
 */
int
cconf_file_read(const char *name,	/* name of file */
		void *buf,	/* buffer */
		int buflen /* length of buf */ );

/**
 * write file
 * write buf into file named 'name'
 * returns:
 * 1 if successful, 0 otherwise.
 */
int
cconf_file_write(const char *name,	/* name of file */
		 const void *buf,	/* buffer */
		 int buflen	/* length of buf */
);

/**
 * append to file
 * append buf at end of file named 'name'
 * returns:
 * 1 if successful, 0 otherwise
 */
int
cconf_file_append(const char *name,	/* name of file */
		  const void *buf,	/* buffer */
		  int buflen /* length of buf */ );

/**
 * append string to file
 * append string at end of file named 'name'
 * returns:
 * 1 if successful, 0 otherwise
 */
int
cconf_file_append_str(const char *name,	/* name of file */
		      const char *str /* string */ );
/**
 * read from temp file descriptor
 * same as cconf_file_read, but instead reads from file descriptor
 * the file descriptor is closed afterwards, in which case the file
 * is deleted (assumed it was previously opened by cconf_file_tmp_fd_write)
 * returns:
 * number of bytes read
 */
int
cconf_file_tmp_fd_read(int fd,	/* file descriptor */
		       void *buf,	/* buffer */
		       int buflen /* length of buf */ );

/**
 * write to temp file descriptor
 * create new a temporary file, and immediately unlink it, write
 * contents of buf into it, and seek() to the beginning of the file.
 * returns:
 * file descriptor
 */
int
cconf_file_tmp_fd_write(const void *buf,	/* buffer */
			int buflen /* size of buf */ );

/**
 * search for string in file
 * poor man's grep, search for string in file.
 * returns:
 * 1 if string is found, 0 otherwise
 */
int
cconf_file_word(const char *file,	/* file name */
		const char *word /* string to search for */ );

#else				/* __STDC__ */

void            cconf_write_config_h();
void            cconf_write_env();

int             cconf_try_run();
int             cconf_try_cc();
int             cconf_try_link();
int             cconf_try_cpp();

int             cconf_check_cc();
int             cconf_check_header();
int             cconf_check_headers();
int             cconf_check_headers_ansi();
int             cconf_check_headers_posix1();
int             cconf_check_headers_xpg3();

int             cconf_check_func();
int             cconf_check_type();
int             cconf_check_type_def();
void            cconf_check_std_types();

int             cconf_file_read();
int             cconf_file_write();
int             cconf_file_append();
int             cconf_file_append_str();
int             cconf_file_tmp_fd_read();
int             cconf_file_tmp_fd_write();
int             cconf_file_word();

#endif				/* !__STDC__ */

#endif
